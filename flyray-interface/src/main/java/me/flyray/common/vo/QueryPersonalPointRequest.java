package me.flyray.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;


/**
 * 个人客户信息
 * @author centerroot
 * @time 创建时间:2018年8月14日下午2:47:21
 * @description
 */

@Data
public class QueryPersonalPointRequest implements Serializable {

    @ApiModelProperty(value = "平台编号")
    private String platformId;

    @ApiModelProperty(value = "个人客户编号")
    private String personalId;

    @ApiModelProperty(value = "客户名称")
    private String realName;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "类型")
    private String tradeType;

    @ApiModelProperty(value = "批次号")
    private String batchNo;

    private Integer currentPage;

    private Integer pageSize;
    
}
