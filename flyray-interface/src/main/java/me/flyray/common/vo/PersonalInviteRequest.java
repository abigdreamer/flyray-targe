package me.flyray.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 查询用户基础信息请求实体
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:23:01
 * @description
 */
@Data
@ApiModel(value = "邀请客户参数请求")
public class PersonalInviteRequest implements Serializable {

	@ApiModelProperty(value = "当前页码")
	private Integer currentPage;

	@ApiModelProperty(value = "每页条数")
	private Integer pageSize;

	@NotNull(message = "参数[personalId]不能为空")
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;
}
