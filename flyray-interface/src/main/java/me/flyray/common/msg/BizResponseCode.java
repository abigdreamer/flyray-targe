package me.flyray.common.msg;

public enum BizResponseCode implements ReturnCode {

	/***********************************通用错误码*****************************************/
    OK("0000", "请求成功"),
    INVALID_FIELDS("100001", "请求参数非法"),
	SYSTEM_ERROR("100002", "系统错误"),
	SERVICE_NOT_AVALIABLE("100003", "服务不可用"),
	SIGNATURE_VERIFY_FAILED("100004", "签名检查失败"),
	BIZ_SYSTEM_ERROR("100005", "业务系统处理异常"),
	CHARACTER_NOT_SUPPORTED("100006", "字符集不被支持"),
	SERVICE_NOT_FOUND("100007", "服务未找到"),
	REQUEST_PATH_ERROR("100008", "访问路径出错"),
	/**
	 * 该错误用于捕捉未被代码识别的或者程序员意料之外的错误，明确已知可能出现的错误不应使用该错误代码
	 */
	SYSTEM_UNKNOWN_ERROR("900001", "系统繁忙"),
    /***********************************token相关错误码*****************************************/
	UOLOAD_FILE_FAIL("400003","文件上传失败"),
	TOKEN_RESOLVEFAIL_CODE("400004","解析Token失败"),
	TOKEN_ISNULL_CODE("400005","token不存在"),
	TOKEN_CHECKFAIL_CODE("400006","验证Token失败"),
	TOKEN_ISNOTVALID_CODE("400007","Token无效"),
	TOKEN_SIGNISNULL_CODE("400008","网关传输参数签名为空"),	
	TOKEN_CHECKSIGNFAIL_CODE("400009","网关校验传输参数签名失败"),
	IPLIST_CHECKFAIL_CODE("400010","白名单检验失败"),
	TOKEN_APPIDISNULL_CODE("400011","获取Token时APPID为空"),
	TOKEN_KEYISNULL_CODE("400012","获取Token时KEY为空"),
	TOKEN_AUTHRESOLVEPARAMS_CODE("400013","授权解析传输参数错误"),
	TOKEN_SALTKEY_ERROR_CODE("400014","授权解析传输参数错误"),
	URL_IS_NULL("400015","请求地址不存在"),
    /*****************************登陆用户相关错误码4001开头六位数字***************************/
    
	/*****************************用户登录错误码***************************/
	USERINFO_NOTEXIST("400100","登录用户不存在"),
	USERNAME_NOTNULL("400101","登录用户不能为空"),
	PASSWORD_NOTNULL("400102","登录密码不能为空"),
	USERINFO_PWD_ERROR("400103","登录密码错误"),
	LOGIN_ERROR("400104","登录失败"),
	TOKEN_ISINVALID("400105","token失效"),
	PARAM_NOTNULL("400106","请求参数不能为空"),
	SIGN_NOTNULL("400107","签名不能为空"),
	SIGN_MATCH_FAIL("400108","签名验证失败"),
	SMS_CODE_ISNULL("400109", "短信验证码不能为空"),
	USER_NO_MENU_AUTH("400110", "用户暂无开通权限"),
    PAY_PWD_CONFIRM_ERROR("400111", "两次支付密码不一致"),
	
	/*****************************用户注册错误码***************************/
	USERINFO_ISEXIST("400120","用户邮箱或手机号已注册"),
	REGISTER_ERROR("400121","注册失败"),
	USERPHONE_ISEXIST("400122","用户已绑定手机号"),
	
	/*****************************商户相关错误码4002开头六位数字****************************/
	
	/*****************************商户认证错误码****************************/
	MERCHANT_ISEXIST("400200","商户已经认证"),
	MERCHANT_FAIL("400201","商户认证失败"),
	APPLYROLE_FAIL("400202","申请失败"),
	CHANGEROLE_FAIL("400203","申请失败"),
	USERLOGINOUT_FAIL("400204","申请失败"),
	DEVICE_NUMBER_EXISTED("400205","设备号已存在"),
	
	/*****************************支付模块错误码4003开头六位数字****************************/
	ORDER_EXIST("400300","订单已存在"),
	NO_AVAILABLE_PAYCHANNEL("400301","无可用支付通道"),
	ORDER_NO_EXIST("400303","订单不存在"),
	ORDER_PAID("400304","订单已支付"),
	INTERFACE_NOTEXIST("400305","接口配置不存在"),
	PAYCHANNEL_INTERFACE_NOTEXIST("400306","支付通道接口不存在"),
	PAYCHANNEL_CONFIGURATION_NOTEXIST("400307","支付通道配置不存在"),
	WECHAT_RESOLVE_FAIL("400308","微信解析失败"),
	SEND_DATA_FAIL("400309","发送数据失败"),
	WECHAT_REFUND_FAIL("400310","微信退款失败"),
	WECHAT_REFUND_TIME_OUT("400311","微信退款超时"),
	PAY_FAIL("400312","支付失败"),
	PAY_NOT_SUCCESS_CAN_NOT_REFUND("400313","订单支付未成功，不能发起退款"),
	ORDER_REFUND("400314","订单已退款"),
	PAY_SERIAL_NOTEXIST("400315","支付流水不存在"),
	PAY_FOR_ANOTHER_FAIL("400316","代付失败"),
	REFUND_AMT_OVER_LIMIT("400317","退款金额大于可退款金额"),
	PAY_CHANNEL_CONFIG_ISEXIST("400318","平台/商户支付通道已存在"),
	REFUND_FAIL("400319","退款失败"),
	WITHDREW_FAIL("400320","提现失败"),
	
	/*****************************crm账户相关错误码4004开头六位数字*****************************************/
	
	/*****************************crm用户架构相关*****************************************/
	DEPT_NO_EXIST("400400","机构不存在"),
	ROLE_NO_EXIST("400401","角色不存在"),
	CUST_NOTEXIST("400402", "用户不存在"),
    MER_NOTEXIST("400403", "商户不存在"),
    PER_NOTEXIST("400404", "会员不存在"),
    PER_ACC_NOTEXIST("400405", "会员账户不存在"),
    MER_ACC_NOTEXIST("400406", "商户账户不存在"),
    USER_FREEZE("400407", "用户被冻结"),
    PER_FREEZE("400408", "会员被冻结"),
    MER_FREEZE("400409", "商户被冻结"),
    PER_ACC_FREEZE("400410", "会员账户被冻结"),
    MER_ACC_FREEZE("400411", "商户账户被冻结"),
	FREEZE_ID_NOTEXIST("400412","冻结流水不存在"),
	FREEZE_UNFREEZE_AMT_INCONSISTEND("400413","解冻金额大于冻结金额"),
    VERIFY_ACC_AMT("400414", "账户余额校验错误"),
    ACC_BALANCE_INSUFFICIENT("400415", "账户余额不足"),
    BIND_CARD_NOTEXIST("400416", "银行卡不存在"),
    PLAT_ACC_TYPE_NOT_EXIST("400417", "平台账户类型不存在"),
    LOCK_USER_PWD("400418", "密码锁定"),
    MOBILE_REPEAT("400419", "手机号已注册"),
    SMS_SEND_FAIL("400420", "短信发送失败"),
    MOBILE_MATCH_FAIL("400421", "手机号错误"),
    SMS_CODE_MATCH_FAIL("400422", "短信验证码错误"),
    OVER_LIMIT("400423","超出限制条数"),
    PWD_ERROR("400424", "密码错误"),
    PAY_PWD_ERROR("400425", "支付密码错误"),
    FEE_VERRIF_ERROR("400426", "手续费校验错误"),
    FEE_LOW_ERROR("400427", "悬赏金额小于平台最低金额"),
    PWD_RESET_ERROR("400428", "密码重置失败"),
    MER_ACC_OPENFAIL("400429", "商户账户开户失败"),
    MER_ACC_INTOACCOUNTFAIL("400430", "商户账户入账失败"),
    PER_ACC_OPENFAIL("400431", "个人账户开户失败"),
    PER_PAYBILL_NOT_EXIST("400432", "支付账单不存在"),
    UNFREEZE_IS_EXIST("400433", "冻结资金已解冻"),
    PER_REFONDBILL_NOT_EXIST("400434", "退款账单不存在"),
    PLATFORM_LOGIN_NAME_EXIST("400435", "平台登录名称不能重复"),
    PER_ACC_BALANCE_NOT_ENOUGH("400436", "用户账户余额不存在"),
    PER_ACC_BALANCE_NOT_MATE("400437", "个人账户余额不匹配"),
    MER_ACC_BALANCE_NOT_MATE("400438", "商户账户余额不匹配"),
    MER_ACC_BALANCE_NOT_ENOUGH("400439", "商户账户余额不足"),
    MER_ACCOUNT_JOURNAL_NOTEXIST("400440", "商户账户流水不存在"),
    PER_WITHDRAWBILL_NOT_EXIST("400441", "提现账单不存在"),
    PER_CERTIFICATION_EXIST("400442", "用户已认证"),
	PLATFORM_SAFETY_CONFIG_NO_EXIST("400443","平台安全信息配置不存在"),
    MER_LOGIN_NAME_EXIST("400444", "商户登录名称不能重复"),
    PERSONALBASE_NOTEXIST("400445", "平台编号和用户编号不能都为空"),
    PER_CERTIFICATION_NOTEXIST("400446", "用户实名认证不存在"),
    PAY_PASSWORD_IS_NULL("400447", "未设置交易密码"),
    SET_PASSWORD_ERROR_CODE("400448", "交易密码设置失败"),
    /*****************************赏金业务相关*****************************************/
    //电话相同简历，被多次投递
    RESUM_REPEAT_SEND_USER("400700", "用户简历重复投递"),
    FUNCTION_NOTEXIST("400701","职能不存在"),
    INDUSTRY_NOTEXIST("400702","行业不存在"),
    ORDER_NOTEXIST("400703", "订单不存在"),
    PAY_ORDER_NOTEXIST("400704","支付订单不存在"),
    FREEZE_ORDER_NOTEXIST("400705", "冻结订单不存在"),
    RESUM_NOTEXIST("400706", "简历不存在"),
    TASK_NOTEXIST("400707", "任务不存在"),
    RESUM_REPEAT_SEND("400708", "简历重复投递"),
    REPEAT_RECEIVER("400709", "任务重复领取"),
	PAY_CREATE_ORDER_FAIL("400710","创建支付订单失败"),
	PAY_INITIATE_PAYMENT_FAIL("400711","发起支付调用失败"),
    RECEIVE_OWN_ERROR("400712", "用户不能领取自己发布的任务"),
    TASK_RECEIVE_NOT("400713", "非运行中的任务不允许领取"),
    SUBFUNCTION_EXIST("400714", "有子职能存在"),
    SUBINDUSTRY_EXIST("400715", "有行业能存在"),
	FUNCTION_EXIST("400716", "记录已经存在"),
	FUNCTION_NO_EXIST("400717", "记录不存在"),
	/*************************爱算相关**********************************************************/
	USER_EXIST("400801", "记录不存在"),
	USER_NUMBER_OVER("400802", "数量已超"),
	
	/*****************************商品二维码业务相关*****************************************/
	GOODS_IS_NULL("400800", "商品不存在"),
	

	/*****************************营销管理*****************************************/
	COUPON_IS_NULL("400900","红包不存在"),
	UNIONID_IS_NULL("400901","平台编号不存在"),
	CASHACBAL_SELECT_FAILED("400902","通过平台编号未查询到用户积分信息"),
	COUPON_USE_SAVE_FAILED("400903","红包使用记录保存失败"),
	INTEGRAL_USE_SAVE_FAILED("400904","积分使用记录保存失败"),
	COUPON_REFUND_SAVE_FAILED("400905","红包退款记录保存失败"),
	INTEGRAL_REFUND_SAVE_FAILED("400906","积分退款记录保存失败"),
	COUPON_IS_EXPIRED("400907","红包已过期"),
	COUPON_REV_FAILED("400908","红包领取失败"),
	COUPON_NOT_REV("400909","红包已领完"),
	INTEGRAL_RECEIVE_FAILED("400910", "积分领取失败"),
	FREEZE_FAILED("400911","冻结失败"),
	UNFREEZE_FAILED("400912","解冻失败"),
	COUPON_MerOrderNo_IS_NULL("400913","订单号不能为空"),
	COUPON_MerOrderNo_IS_OK("400914","订单号无效"),
	
	
	//==========================公积金=============================
	GJJ_INSERT_ERROR("400915","新增信息失败"),
	GJJ_UPDATE_ERROR("400916","更新信息失败"),
	MEMBERMODIFY_NULL_CODE("400917","修改用户信息不存在"),
	MEMBERMODIFY_ERROR_CODE("400918","修改用户信息失败"),
	
	
	/********************************** *******************************************************/
	REV_COUNT_OVER("今日领取次数已达上限","401004"),
	BIZ_ACCOUNT_STATUT_CHECK_ERROR("401005","已通过审核账户流水不能再次审核操作"),
	
	/********************************** 订单状态查询*******************************************************/
	ACCOUNT_TRANSACTION_STATUS_FAILED("401101","交易状态失败")
	;
	
    private String code;
    private String message;

	private BizResponseCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String getReturnCode() {
		return code;
	}

	@Override
	public String getReturnMessage() {
		return message;
	}

}
