package me.flyray.common.msg;

/**
 * 
 * 查询红包使用状态 需要转换新的红包使用场景。比如 ：手机充值和流量充值， 合并成“话费流量”一个“场景”
 * @author mu
 *
 */
public enum NewSceneType {
	
	PHONE_RECHARGE("BT0004","话费"),
	FLOW_PAYMENT("BT0021","流量"),
	MEDICAL_PAYMENT("BT0007","医疗"),
	REGISTER_PAYMENT("BT0008","医疗"),
	OUTPATIENT_RECHARGE("BT0024","医疗"),
	
	ADD_READER("BT0009","图书馆"),
	UP_READER("BT0011","图书馆"),
	
	WATER_PAYMENT("BT0017","水电煤"),
	ELECTRICITY_PAYMENT("BT0018","水电煤"),
	GAS_PAYMENT("BT0019","水电煤"),
	
	VIDEO_CARD("BT0020","视频卡"),
	
	MEALCARD_RECHARGE("BT0022","餐卡"),
	
	ZB_RECHARGE("BT0023","中百卡"),
	
	BUS_RECHARGE("BT0025","公交卡"),
	
	SCENIC_PURCHASE("BT0026","景区"),
	
	AUTO_SALE("BT0027","自助售货机");
	
	
	private String code;
	private String name;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private NewSceneType(String code, String name) {
		this.code = code;
		this.name = name;
	}
	
	public NewSceneType getSceneType(String code) {
		for (NewSceneType s : NewSceneType.values()) {
			if (s.getCode().equals(code)) {
				return s;
			}
		}
		return null;
	}
	
	public static String getSceneTypename(String code) {
		for (NewSceneType s : NewSceneType.values()) {
			if (s.getCode().equals(code)) {
				return s.name;
			}
		}
		return "";
	}
	
	public static void main(String[] args) {
		//System.out.println(NewSceneType.getSceneTypename("BT0021"));
		
		String sceneIdStr ="BT0023";
		StringBuffer stringBuffer = new StringBuffer("");
		String str="";
		if(null != sceneIdStr && !"".equals(sceneIdStr)){
			String[]sceneIds=sceneIdStr.split(",");
			if(null!=sceneIds&&sceneIds.length>0){
				for(String scence:sceneIds){
					if(!stringBuffer.toString().contains(NewSceneType.getSceneTypename(scence))){
						stringBuffer.append(NewSceneType.getSceneTypename(scence)).append(",");//场景中文
					}
				}
				str=stringBuffer.toString().substring(0,stringBuffer.toString().lastIndexOf(","));
				System.out.println(str);
			}
		}
	}
	
	
	

}
