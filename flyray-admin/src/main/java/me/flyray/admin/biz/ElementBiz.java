package me.flyray.admin.biz;

import me.flyray.admin.entity.Element;
import me.flyray.admin.mapper.ElementMapper;
import me.flyray.common.biz.BaseBiz;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ElementBiz extends BaseBiz<ElementMapper, Element> {

    public List<Element> getAuthorityElementByUserId(String userId){
        return mapper.selectAuthorityElementByUserId(userId);
    }

    public List<Element> getAllElementPermissions(){
        return mapper.selectAllElementPermissions();
    }

    @Override
    public int insertSelective(Element entity) {
        return super.insertSelective(entity);
    }

    @Override
    public void updateSelectiveById(Element entity) {
        super.updateSelectiveById(entity);
    }
}
