package me.flyray.admin.biz;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.flyray.admin.constant.AdminCommonConstant;
import me.flyray.admin.entity.Element;
import me.flyray.admin.entity.Menu;
import me.flyray.admin.entity.ResourceAuthority;
import me.flyray.admin.entity.Role;
import me.flyray.admin.vo.AuthorityMenuTree;
import me.flyray.admin.mapper.*;
import me.flyray.common.biz.BaseBiz;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.util.EntityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.*;

/** 
* @author: bolei
* @date：2018年4月8日 下午4:13:06 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class RoleBiz extends BaseBiz<RoleMapper, Role> {
	
	@Autowired
    private UserMapper userMapper;
    @Autowired
    private ResourceAuthorityMapper resourceAuthorityMapper;
    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private ElementMapper elementMapper;
    @Autowired
    private RoleMapper roleMapper;

    public Map<String, Object> add(Map<String, Object> param) throws Exception{
    	
    	Role role = EntityUtils.map2Bean(param, Role.class);
    	role.setCreateTime(new Date());
    	mapper.insert(role);
    	Map<String, Object> result = new HashMap<String, Object>();
    	result.put("code", ResponseCode.OK.getCode());
    	result.put("message", ResponseCode.OK.getMessage());
    	result.put("success", true);
    	result.put("role", role);
    	return result;
	}
    
	public void modifyRoleUsers(int id, String members, String leaders) {
		// TODO Auto-generated method stub
		
	}

	/**
     * 变更role关联的菜单
     *
     * @param roleId
     * @param menus
     */
    public void modifyAuthorityMenu(String roleId, String appId, String[] menus) {
        //删除授予角色的权限
        resourceAuthorityMapper.deleteByAuthorityIdAndResourceType(roleId,appId, AdminCommonConstant.RESOURCE_TYPE_MENU);
        List<Menu> menuList = menuMapper.selectAll();
        Map<String, String> map = new HashMap<String, String>();
        for (Menu menu : menuList) {
            map.put(menu.getId().toString(), menu.getParentId().toString());
        }
        Set<String> relationMenus = new HashSet<String>();
        relationMenus.addAll(Arrays.asList(menus));
        ResourceAuthority authority = null;
        for (String menuId : menus) {
            findParentID(map, relationMenus, menuId);
        }
        //插入新的角色权限
        for (String menuId : relationMenus) {
            authority = new ResourceAuthority(AdminCommonConstant.AUTHORITY_TYPE_GROUP, AdminCommonConstant.RESOURCE_TYPE_MENU);
            authority.setAuthorityId(roleId);
            authority.setResourceId(menuId);
            authority.setAppId(appId);
            authority.setParentId("-1");
            if (!"-1".equals(menuId)){
                resourceAuthorityMapper.insertSelective(authority);
            }
        }
    }
    
    private void findParentID(Map<String, String> map, Set<String> relationMenus, String id) {
        String parentId = map.get(id);
        if (String.valueOf(AdminCommonConstant.ROOT).equals(id)) {
            return;
        }
        if (!String.valueOf(AdminCommonConstant.ROOT).equals(parentId)){
            relationMenus.add(parentId);
        }
        findParentID(map, relationMenus, parentId);
    }


	
	public Object getRoleUsers(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
     * 给role分配按钮级别资源权限
     *
     * @param roleId
     * @param menuId
     * @param elementId
     */
    @CacheEvict(value = {"permission-ele","permission-u"})
    public void modifyAuthorityElement(int roleId, String appId, int menuId, int elementId) {
        ResourceAuthority authority = new ResourceAuthority(AdminCommonConstant.AUTHORITY_TYPE_GROUP, AdminCommonConstant.RESOURCE_TYPE_BTN);
        authority.setAppId(appId);
        authority.setAuthorityId(roleId + "");
        authority.setResourceId(elementId + "");
        authority.setParentId("-1");
        resourceAuthorityMapper.insertSelective(authority);
    }

    /**
     * 移除给role分配按钮级别资源权限
     *
     * @param roleId
     * @param menuId
     * @param elementId
     */
    public void removeAuthorityElement(int roleId, int menuId, int elementId) {
        ResourceAuthority authority = new ResourceAuthority();
        authority.setAuthorityId(roleId + "");
        authority.setResourceId(elementId + "");
        authority.setParentId("-1");
        resourceAuthorityMapper.delete(authority);
    }

    /**
     * 获取该角色授权的按钮或功能元素
     * @param id
     * @return
     */
	public Object getAuthorityElement(int id) {
		return elementMapper.selectAuthorityElementByRoleId(id);
	}
	
	/**
	 * 获取该角色某个菜单下得授权按钮或功能元素
	 * @param id
	 * @param menuId
	 * @return
	 */
	public List<Element> getAuthorityMenuElement(int roleId, int menuId) {
		return elementMapper.selectAuthorityElementByRoleIdAndMenuId(roleId,menuId);
	}

	public TableResultResponse<Role> queryList(Map<String, Object> params) {
		Example example = new Example(Role.class);
		Criteria criteria = example.createCriteria();
		Object platformId = params.get("platformId");
		Object deptId = params.get("deptId");
		if(!"".equals(platformId) && null != platformId){
			//是平台管理员
			criteria.andEqualTo("platformId", platformId);
		}else {
			//系统管理员
		}
		
		if(!"".equals(deptId) && null != deptId){
			criteria.andEqualTo("deptId", deptId);
		}
		
		Page<Role> result = PageHelper.startPage(Integer.valueOf((String)(params.get("page"))), Integer.valueOf((String)params.get("limit")));
		List<Role> list = roleMapper.selectByExample(example);
		return new TableResultResponse<Role>(result.getTotal(), list);
	}

}
