package me.flyray.admin.mapper;

import java.util.List;

import me.flyray.admin.entity.UserRole;
import tk.mybatis.mapper.common.Mapper;

public interface UserRoleMapper extends Mapper<UserRole> {
	
	List<UserRole> getUserRolesByUserId(Long id);
	
}