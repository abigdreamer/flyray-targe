package me.flyray.admin.api;

import me.flyray.admin.biz.DeptBiz;
import me.flyray.admin.biz.UserBiz;
import me.flyray.admin.constant.AdminCommonConstant;
import me.flyray.admin.entity.User;
import me.flyray.admin.mapper.UserMapper;
import me.flyray.admin.vo.OrgStructureNode;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.rest.BaseController;
import me.flyray.common.vo.admin.AdminUserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: bolei
 * @date: 17:50 2019/3/19
 * @Description: 类描述
 */

@RestController
@RequestMapping("user/api")
public class UserApiController extends BaseController<UserBiz, User> {

    @Autowired
    private DeptBiz deptBiz;
    @Autowired
    private UserMapper userMapper;

    /**
     * 给业务系统调用的
     * @param bean
     * @return
     */
    @RequestMapping(value = "/pageList",method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<User> apiPageList(@RequestBody AdminUserRequest bean){
        bean.setPlatformId(setPlatformId(bean.getPlatformId()));
        return baseBiz.pageList(bean);
    }

    /**
     * 修改用户手机号
     * @author centerroot
     * @time 创建时间:2018年11月21日上午11:35:36
     * @param param
     * @return
     */
    @RequestMapping(value = "/orgStructure", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<OrgStructureNode> getTree(@RequestParam Map<String, Object> param) {

        //更加平台编号和parentId 为-1查询出该平台下的顶级部门编号deptId
        String parentId = String.valueOf(AdminCommonConstant.ROOT);
        List<User> users = userMapper.selectAll();
        List<OrgStructureNode> orgStructureNodes = new ArrayList<>();
        for (User user : users){
            OrgStructureNode orgStructureNode = new OrgStructureNode();
            orgStructureNode.setId(user.getUserId());
            orgStructureNode.setParentId(user.getParentId());
            orgStructureNode.setName(user.getUsername());
            orgStructureNode.setValue(user.getUserId());
            orgStructureNodes.add(orgStructureNode);
        }

        orgStructureNodes =  getChild(parentId,orgStructureNodes);
        OrgStructureNode result = new OrgStructureNode();
        result.setName("管理员");
        result.setValue("1");
        result.setChildren(orgStructureNodes);
        return BaseApiResponse.newSuccess(result);
    }

    /**
     * 获取子节点
     * @param id 父节点id
     * @param allUser 所有用户列表
     * @return 每个根节点下，所有子用户列表
     */
    public List<OrgStructureNode> getChild(String id,List<OrgStructureNode> allUser){
        //子菜单
        List<OrgStructureNode> childList = new ArrayList<OrgStructureNode>();
        for (OrgStructureNode nav : allUser) {
            // 遍历所有节点，将所有菜单的父id与传过来的根节点的id比较
            //相等说明：为该根节点的子节点。
            if(nav.getParentId().equals(id)){
                childList.add(nav);
            }
        }
        //递归
        for (OrgStructureNode nav : childList) {
            nav.setChildren(getChild(nav.getId(), allUser));
        }
        //如果节点下没有子节点，返回一个空List（递归退出）
        if(childList.size() == 0){
            return new ArrayList<OrgStructureNode>();
        }
        return childList;
    }

}
