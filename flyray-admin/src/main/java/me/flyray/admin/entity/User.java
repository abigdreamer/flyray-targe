package me.flyray.admin.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@Data
@JsonInclude(value= JsonInclude.Include.ALWAYS )
@Table(name = "base_user")
public class User {

    @Column(name = "id")
    private Integer id;
    @Id
    @Column(name = "user_id")
    private String userId;

    @Column(name = "parent_id")
    private String parentId;

    @Column(name = "parent_name")
    private String parentName;

    private String username;

    @JsonInclude(value= JsonInclude.Include.ALWAYS )
    private String password;

    /**
     * 所属部门机构ID
     */
    @Column(name = "dept_id")
    private String deptId;

    /**
     * 所属部门机构名称
     */
    @Column(name = "dept_name")
    private String deptName;

    private String birthday;

    private String address;

    @Column(name = "mobile_phone")
    private String mobilePhone;

    @Column(name = "tel_phone")
    private String telPhone;

    private String email;

    private String sex;

    private String status;

    private String description;

    @Column(name = "crt_time")
    private Date crtTime;

    @Column(name = "crt_user")
    private String crtUser;

    @Column(name = "crt_name")
    private String crtName;

    @Column(name = "crt_host")
    private String crtHost;

    @Column(name = "upd_time")
    private Date updTime;

    @Column(name = "upd_user")
    private String updUser;

    @Column(name = "upd_name")
    private String updName;

    @Column(name = "upd_host")
    private String updHost;

    /**
     * 平台编号
     */
    @Column(name = "platform_id")
    private String platformId;

    /**
     * 商户编号
     */
    @Column(name = "merchant_id")
    private String merchantId;

    /**
     * 用户权限1，系统管理员2，平台级管理员3，商户级管理员4，平台操作员，5，代理商级管理员
     */
    @Column(name = "user_type")
    private Integer userType;

    /**
     * 用户业务角色类型
     */
    @Column(name = "user_biz_role")
    private Integer userBizRole;

    /**
     * 省
     */
    @Column(name = "area_code")
    private String areaCode;
    
    /**
     * 市
     */
    @Column(name = "area_name")
    private String areaName;
    
    /**
     * 县
     */
    @Column(name = "area_layer")
    private String areaLayer;

    @Transient
    private String roleId;


}