package me.flyray.crm.facade.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author: bolei
 * @date: 10:50 2019/2/14
 * @Description: 代理商添加请求类
 */

@Data
public class CommercialAgentRequest implements Serializable {

    /**
     * 平台编号
     */
    @NotNull(message="平台编号不能为空")
    private String platformId;

    /**
     * 部门机构ID
     */
    @NotNull(message="部门ID不能为空")
    private String deptId;

    /**
     * 部门机构名称
     */
    @NotNull(message="部门名称不能为空")
    private String deptName;

    /**
     * 代理商登陆名称
     */
    @NotNull(message="代理商登陆名称不能为空")
    private String username;

    /**
     * 代理商登陆名称
     */
    private String password;

    /**
     * 用户ID
     */
    private String userId;

    private String phone;

    //用户名称
    private String realName;

    //身份证号
    private String idCard;

    //用户昵称
    private String nickName;

    //性别 1：男 2：女
    private String sex;

    //生日
    private String birthday;


    //居住地
    private String address;

    //身份证正面
    private String idPositive;

    //身份证反面
    private String idNegative;

    //认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
    private String authenticationStatus;

    //账户状态 00：正常，01：冻结
    private String status;

    //创建时间
    private Date createTime;

    //更新时间
    private Date updateTime;

    // 归属人（存后台管理系统登录人员id）指谁发展的客户
    private Long owner;

    // 用户头像
    private String avatar;

    //备注
    private String remark;

    //个人客户类型  00：注册用户  01：运营添加用户
    private String personalType;



}
