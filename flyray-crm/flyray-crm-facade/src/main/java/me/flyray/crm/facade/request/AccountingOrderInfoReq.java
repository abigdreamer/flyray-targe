package me.flyray.crm.facade.request;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 查询订单状态
 * @author yyn
 * @time 创建时间:2018年12月28日上午11:13:20
 * @description
 */
public class AccountingOrderInfoReq implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * 合作平台编号
	 */
	@NotNull(message="合作平台编号不能为空")
	private String platformId;
	
	/**
	 * 商户订单号
	 */
	@NotNull(message="商户订单号不能为空")
	private String orderNo;

	/**
	 * 交易类型
	 */
	@NotNull(message="交易类型不能为空")
	private String tradeType;

	/**
	 * 是否余额账户
	 */
	@NotNull(message="是否余额账户字段不能为空")
	private String isBalance;

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getIsBalance() {
		return isBalance;
	}

	public void setIsBalance(String isBalance) {
		this.isBalance = isBalance;
	}

	@Override
	public String toString() {
		return "AccountingOrderInfoReq [acceptBizNo=" + platformId + ", merOrderNo=" + orderNo + ", tradeType="
				+ tradeType + ", isBalance=" + isBalance + "]";
	}
}
