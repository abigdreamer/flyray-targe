package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;


/**
 * 个人客户基础信息请求实体
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:24:07
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "个人客户基础信息请求参数")
public class PersonalBaseRequest implements Serializable {
	
	@ApiModelProperty(value = "序号")
    private Long id;

//	@NotNull(message="个人客户编号不能为空")
	@ApiModelProperty(value = "个人客户编号")
    private String personalId;
	
    @NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	@ApiModelProperty(value = "用户编号")
    private String customerId;
	
	@ApiModelProperty(value = "第三方会员编号")
    private String thirdNo;
	
	@ApiModelProperty(value = "用户名称")
    private String realName;
	
	@ApiModelProperty(value = "身份证号")
    private String idNumber;
	
	@ApiModelProperty(value = "用户昵称")
    private String nickname;
	
    @ApiModelProperty(value = "性别")
    private String sex;
	
	@ApiModelProperty(value = "生日")
    private String birthday;
	
	@ApiModelProperty(value = "居住地")
    private String address;
	
	@ApiModelProperty(value = "身份证正面")
    private String idPositive;
	
	@ApiModelProperty(value = "身份证反面")
    private String idNegative;
	
	@ApiModelProperty(value = "认证状态")
    private String authenticationStatus;
	
	@ApiModelProperty(value = "账户状态")
    private String status;
	
	@ApiModelProperty(value = "创建时间")
    private Date createTime;
	
	@ApiModelProperty(value = "更新时间")
    private Date updateTime;
	
	// 归属人（存后台管理系统登录人员id）指谁发展的客户
	@ApiModelProperty(value = "归属人")
    private String ownerId;

	private String ownerName;

	@ApiModelProperty(value = "是否为客户经理 1：是，0：否")
	private Integer isOwner;

    // 用户头像
	@ApiModelProperty(value = "用户头像")
    private String avatar;
	
    //个人客户类型  00：注册用户  01：运营添加用户
	@ApiModelProperty(value = "个人客户类型")
	private String personalType;
	
	//手机号
	@ApiModelProperty(value = "手机号")
	private String phone;
	
	//备注
	@ApiModelProperty(value = "备注")
	private String remark;
    
	    //客户等级
	@ApiModelProperty(value = "客户等级")
    private String personalLevel;
	
	    //行业
	@ApiModelProperty(value = "行业")
    private String industry;
	
	    //行业描述
	@ApiModelProperty(value = "行业描述")
    private String industryDescription;
	
	    //职务
	@ApiModelProperty(value = "职务")
    private String job;
	
	    //所在省市
	@ApiModelProperty(value = "所在省市")
    private String provinces;
	
	    //电子邮件
	@ApiModelProperty(value = "电子邮件")
    private String email;
	
	    //邮编
	@ApiModelProperty(value = "邮编")
    private String zipCode;

}
