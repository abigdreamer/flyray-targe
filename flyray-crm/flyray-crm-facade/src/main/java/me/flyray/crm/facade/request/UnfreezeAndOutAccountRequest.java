package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("个人或者商户解冻并出账接口")
public class UnfreezeAndOutAccountRequest implements Serializable {

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message = "解冻金额不能为空")
	@ApiModelProperty("解冻金额")
	private String unfreezeAmt;

	@NotNull(message = "客户编号不能为空")
	@ApiModelProperty("客户编号")
	private String customerId;

	@ApiModelProperty("商户编号")
	private String merchantId;

	@ApiModelProperty("商户名称")
	private String merchantName;
	
	@ApiModelProperty("商户类型编号")
	private String merchantType;
	
	@NotNull(message = "客户类型不能为空")
	@ApiModelProperty("客户类型")//01:商户 02:个人
	private String customerType;
	
	@NotNull(message = "账户类型不能为空")
	@ApiModelProperty("账户类型")
	private String accountType;

	@NotNull(message = "账户名称不能为空")
	@ApiModelProperty("账户名称")
	private String accountName;
	
	@NotNull(message = "冻结流水号不能为空")
	@ApiModelProperty("冻结流水号")
	private String freezeId;
	
	@NotNull(message = "订单号不能为空")
	@ApiModelProperty("订单号")
	private String orderNo;

	@ApiModelProperty("个人客户编号")
	private String personalId;

	@ApiModelProperty("个人客户名称")
	private String personalName;
	
	@NotNull(message = "交易类型不能为空")
	@ApiModelProperty("交易类型")
	private String tradeType;

	@ApiModelProperty("去向资金账号")
	private String toAccount;

	@ApiModelProperty("去向资金账号名称")
	private String toAccountName;

	private Integer roleType;
	
}
