package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;

/**
 * @Author: bolei
 * @date: 20:49 2019/2/15
 * @Description: 要求请求参数
 */

@Data
public class DistributionInviteRequest {

    @NotNull(message="平台编号不能为空")
    @ApiModelProperty(value = "平台编号")
    private String platformId;

    /**
     * 邀请人编号
     */
    @NotNull(message="邀请人编号不能为空")
    @ApiModelProperty(value = "邀请人编号")
    private String inviterPersonalId;

    /**
     * 邀请人openid
     */
    private String inviterOpenId;

    /**
     * 被邀请人personalId
     */
    @NotNull(message="被邀请人编号不能为空")
    @ApiModelProperty(value = "被邀请人编号")
    private String inviteePersonalId;

    /**
     * 被邀请人openid
     */
    private String inviteeOpenId;

    /**
     * 被邀请人phone
     */
    private String inviteePhone;

    /**
     * 被邀请人姓名
     */
    private String inviteeRealName;

    /**
     * 被邀请人性别
     */
    private String inviteeSex;

    /**
     * 被邀请人email
     */
    private String inviteeEmail;

}
