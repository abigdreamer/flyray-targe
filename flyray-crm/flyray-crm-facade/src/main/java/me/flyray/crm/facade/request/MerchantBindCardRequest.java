package me.flyray.crm.facade.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author: bolei
 * @date: 18:33 2019/1/7
 * @Description: 财务信息
 */

@Data
public class MerchantBindCardRequest implements Serializable {

    private Long id;
    //平台号
    private String platformId;

    //客户ID
    private String customerId;

    //商户ID
    private String merchantId;

    //MER01:企业商户，MER02：个人商户，MER00：企业
    private String merchantType;

    //身份证号
    private String idCardNo;

    //身份证正面照片
    private String idCardFrontImg;

    //身份证反面照片
    private String idCardBackImg;

    //持卡人姓名
    private String cardholderName;

    //收款银行名称
    private String beneficiaryBankName;

    //收款人卡号
    private String beneficiaryBankCardNo;

    //加密银行卡号
    private String encryptBankCardNo;

    //状态  00：待验证  01：绑定  02：解绑
    private String status;

    //收款银行支行名称
    private String beneficiaryBankBranchName;

    //支行编号
    private String beneficiaryBranchNo;

    //创建时间
    private Date createTime;

    //更新时间
    private Date updateTime;

    /**
     * 为了区分不同角色的商户
     */
    private Integer roleType;

    /**
     * 银行开户许可证
     */
    private String accountPermitsImg;

    /**
     * 营业执照编号
     */
    private String businessLisence;
    /**
     * 营业执照
     */
    private String lisenceImageUrl;

}
