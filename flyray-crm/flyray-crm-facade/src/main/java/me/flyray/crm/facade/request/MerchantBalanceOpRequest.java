package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Auther: luya
 * @Date: 2019-01-16 21:53
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "运营查询商户账户信息请求参数")
public class MerchantBalanceOpRequest implements Serializable {

    //平台编号
    @NotNull(message="平台编号不能为空")
    @ApiModelProperty(value = "平台编号")
    private String platformId;

    //商户编号
    @ApiModelProperty(value = "商户编号")
    private String customerId;

    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户
    @ApiModelProperty(value = "客户类型")
    private String merchantType;

    private Integer roleType;

}
