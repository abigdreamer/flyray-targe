package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("修改手机号信息参数")
public class ModifyCustomerMobileParam implements Serializable {
	@NotNull(message="用户信息编号不能为空")
	@ApiModelProperty("用户信息编号")
	private String customerId;
	@NotNull(message="旧手机号不能为空")
	@ApiModelProperty("旧手机号")
	private String oldMobile;
	@NotNull(message="旧手机号短信验证码不能为空")
	@ApiModelProperty("旧手机号短信验证码")
	private String oldSmsCode;
	@NotNull(message="新手机号不能为空")
	@ApiModelProperty("新手机号")
	private String newMobile;
	@NotNull(message="新手机号短信验证码不能为空")
	@ApiModelProperty("新手机号短信验证码")
	private String newSmsCode;
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
}
