package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Auther: luya
 * @Date: 2019-01-16 21:53
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "运营查询商户账户信息请求参数")
public class MerchantAccountOpRequest {

    @ApiModelProperty(value = "当前页码")
    private int page =1;

    @ApiModelProperty(value = "每页条数")
    private int limit =10 ;

    //账户编号
    @ApiModelProperty(value = "账户编号")
    private String customerId;

    //平台编号
    @NotNull(message="平台编号不能为空")
    @ApiModelProperty(value = "平台编号")
    private String platformId;

    @ApiModelProperty(value = "商户名称")
    private String merchantName;

    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户
    @ApiModelProperty(value = "客户类型")
    private String merchantType;

    //账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：冻结金额账户，ACC008：平台管理费账户，ACC009：平台服务费账户等......
    @ApiModelProperty(value = "账户类型")
    private String accountType;

    private Date startDate;

    private Date endDate;
    @ApiModelProperty(value = "商户角色，1：流量主，2：广告主")
    private Integer roleType;
}
