package me.flyray.crm.core.modules.personal;

import lombok.extern.slf4j.Slf4j;
import me.flyray.common.context.BaseContextHandler;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.util.EntityUtils;
import me.flyray.common.vo.*;
import me.flyray.crm.core.biz.personal.PersonalBaseExtBiz;
import me.flyray.crm.core.util.PageResult;
import me.flyray.crm.facade.request.PersonalBaseAddRequest;
import me.flyray.crm.facade.request.PersonalBaseModifyRequest;
import me.flyray.crm.facade.request.WechatUnBindRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("personalBase/ext")
public class PersonalBaseExtController {

    @Autowired
    private PersonalBaseExtBiz personalBaseExtBiz;

    /**
     * 查询个人及积分基础信息
     * @author centerroot
     * @time
     * @param request
     * @return
     */
    @RequestMapping(value = "/queryWithPointsList", method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<QueryPersonalInfoExtResponse> queryWithPointsList(@RequestBody @Valid QueryPersonalBaseExtRequest request){

        try {
            PageResult pageResult = new PageResult();
            request.setUserId(BaseContextHandler.getXId());
            request.setUserType(BaseContextHandler.getUserType());
            if(StringUtils.isNotBlank(BaseContextHandler.getPlatformId())){
                request.setPlatformId(BaseContextHandler.getPlatformId());
            }
            List<QueryPersonalInfoExtResponse> result = personalBaseExtBiz.queryWithPointsList(request, pageResult);
            return new TableResultResponse<QueryPersonalInfoExtResponse>(pageResult.getRowCount(), result);
        } catch (Exception e) {
            log.error("PersonalBaseExtController queryWithPointsList, request={}, Exception = {}", EntityUtils.beanToMap(request), e);
            TableResultResponse<QueryPersonalInfoExtResponse> response = new TableResultResponse<QueryPersonalInfoExtResponse>();
            response.setCode(ResponseCode.SYSTEM_ERROR.getCode());
            response.setMessage(ResponseCode.SYSTEM_ERROR.getMessage());
            return response;
        }
    }


    /**
     * 更新客户等级
     * @author centerroot
     * @time
     * @param request
     * @return
     */
    @RequestMapping(value = "/updatePersonalLevels", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse updatePersonalLevels(@RequestBody @Valid PersonalLevelRequest request){

        try {
            return  personalBaseExtBiz.updatePersonalLevels(request);
        } catch (Exception e) {
            log.error("PersonalBaseExtController updatePersonalLevels, request={}, Exception = {}", EntityUtils.beanToMap(request), e);
            return  BaseApiResponse.newFailure( ResponseCode.SYSTEM_ERROR);
        }
    }

    /**
     * 查询个人及积分基础信息
     * @author centerroot
     * @time
     * @param request
     * @return
     */
    @RequestMapping(value = "/detail", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse queryPersonalDetail(@RequestBody @Valid PersonalDetailRequest request){

        try {
            return  personalBaseExtBiz.queryPersonalDetail(request);
        } catch (Exception e) {
            log.error("PersonalBaseExtController queryPersonalDetail, request={}, Exception = {}", EntityUtils.beanToMap(request), e);
            return  BaseApiResponse.newFailure( ResponseCode.SYSTEM_ERROR);
        }
    }

    @RequestMapping(value = "/queryInvitePersonal", method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<PersonalInviteResponse>  queryInvitePersonal(@RequestBody @Valid PersonalInviteRequest request){

        try {
            PageResult pageResult = new PageResult();
            List<PersonalInviteResponse> result = personalBaseExtBiz.queryInvitePersonal(request, pageResult);
            return new TableResultResponse<PersonalInviteResponse>(pageResult.getRowCount(), result);
        } catch (Exception e) {
            log.error("PersonalBaseExtController queryInvitePersonal, request={}, Exception = {}", EntityUtils.beanToMap(request), e);
            TableResultResponse<PersonalInviteResponse> response = new TableResultResponse<PersonalInviteResponse>();
            response.setCode(ResponseCode.SYSTEM_ERROR.getCode());
            response.setMessage(ResponseCode.SYSTEM_ERROR.getMessage());
            return response;
        }
    }

    /**
     * 更新客户经理
     * @author centerroot
     * @time
     * @param request
     * @return
     */
    @RequestMapping(value = "/updatePersonalOwner", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse updatePersonalOwner(@RequestBody @Valid PersonalOwnerRequest request){

        try {
            return  personalBaseExtBiz.updatePersonalOwner(request);
        } catch (Exception e) {
            log.error("PersonalBaseExtController updatePersonalOwner, request={}, Exception = {}", EntityUtils.beanToMap(request), e);
            return  BaseApiResponse.newFailure( ResponseCode.SYSTEM_ERROR);
        }
    }



    /**
     * 接触微信绑定关系
     * @time 创建时间:2018年7月16日下午6:02:15
     * @param request
     * @return
     */
    @RequestMapping(value = "/unBindWechat", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse unBindWechat(@RequestBody @Valid WechatUnBindRequest request){

        try {
            return  personalBaseExtBiz.unBindWechat(request);
        } catch (Exception e) {
            log.error("PersonalBaseExtController unBindWechat, request={}, Exception = {}", EntityUtils.beanToMap(request), e);
            return  BaseApiResponse.newFailure( ResponseCode.SYSTEM_ERROR);
        }
    }

    /**
     * 获取客户等级列表
     * @time 创建时间:2018年7月16日下午6:02:15
     * @param
     * @return
     */
    @RequestMapping(value = "/getPersonalLevelList", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse getPersonalLevelList(){

        try {
            return  personalBaseExtBiz.getPersonalLevelList();
        } catch (Exception e) {
            log.error("PersonalBaseExtController getPersonalLevelList, Exception = {}", e);
            return  BaseApiResponse.newFailure( ResponseCode.SYSTEM_ERROR);
        }
    }

    /**
     * 获取客户等级列表
     * @time 创建时间:2018年7月16日下午6:02:15
     * @param
     * @return
     */
    @RequestMapping(value = "/getOwnerList", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse getOwnerList(){

        try {
            String userId = BaseContextHandler.getXId();
            String userType = BaseContextHandler.getUserType();
            String platformId = BaseContextHandler.getPlatformId();
            return  personalBaseExtBiz.getOwnerList(userId, userType, platformId);
        } catch (Exception e) {
            log.error("PersonalBaseExtController getPersonalLevelList, Exception = {}", e);
            return  BaseApiResponse.newFailure( ResponseCode.SYSTEM_ERROR);
        }
    }

    /**
     * 更新客户经理
     * @author centerroot
     * @time
     * @param request
     * @return
     */
    @RequestMapping(value = "/addPersonalPoints", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse addPersonalPoints(@RequestBody @Valid PersonalPointsRequest request){

        try {
            request.setPlatformId(BaseContextHandler.getPlatformId());
            return  personalBaseExtBiz.addPersonalPoints(request);
        } catch (Exception e) {
            log.error("PersonalBaseExtController addPersonalPoints, request={}, Exception = {}", EntityUtils.beanToMap(request), e);
            return  BaseApiResponse.newFailure( ResponseCode.SYSTEM_ERROR);
        }
    }


}