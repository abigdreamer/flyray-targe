package me.flyray.crm.core.modules.personal;

import java.util.Map;

import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;
import me.flyray.common.context.BaseContextHandler;
import me.flyray.common.enums.PointsTradeType;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.msg.ReturnCode;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.util.EntityUtils;
import me.flyray.common.vo.QueryPersonalPointRequest;
import me.flyray.crm.core.entity.PersonalAccount;
import me.flyray.crm.core.entity.PersonalPointJournal;
import me.flyray.crm.facade.request.PersonalAccountRequest;
import me.flyray.crm.facade.response.PersonalPointJournalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.personal.PersonalAccountBiz;

@RestController
@RequestMapping("personalAccount")
@Slf4j
public class PersonalAccountController extends BaseController<PersonalAccountBiz, PersonalAccount> {

	@Autowired
	private PersonalAccountBiz personalAccountBiz;

	/**
	 * 查询个人账户信息列表
	 * @author centerroot
	 * @time 创建时间:2018年8月15日上午10:32:02
	 * @param personalAccountRequest
	 * @return
	 */
	@RequestMapping(value = "/queryList", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryList(@RequestBody @Valid PersonalAccountRequest personalAccountRequest){
		personalAccountRequest.setPlatformId(setPlatformId(personalAccountRequest.getPlatformId()));
		return baseBiz.queryList(personalAccountRequest);
	}

	@RequestMapping(value = "/pointList", method = RequestMethod.POST)
	@ResponseBody
	public TableResultResponse<PersonalPointJournalResponse> queryList(@RequestBody @Valid QueryPersonalPointRequest queryPersonalPointRequest){

		try {
			if (queryPersonalPointRequest != null && queryPersonalPointRequest.getTradeType() != null){
				if (queryPersonalPointRequest.getTradeType().equals("1")) {
					// 业务奖励积分
					queryPersonalPointRequest.setTradeType(PointsTradeType.POINTS_BIZ.getCode());
				} else if (queryPersonalPointRequest.getTradeType().equals("2")) {
					//推荐奖励积分
					queryPersonalPointRequest.setTradeType(PointsTradeType.POINTS_RECOMMEND.getCode());
				} else if (queryPersonalPointRequest.getTradeType().equals("3")) {
					// 默认：积分冲抵
					queryPersonalPointRequest.setTradeType(PointsTradeType.POINTS_DEDUCTION.getCode());
				} else if (queryPersonalPointRequest.getTradeType().equals("4")) {
					// 默认：积分冲抵
					queryPersonalPointRequest.setTradeType(PointsTradeType.POINTS_EXCHANGE.getCode());
				} else {
					queryPersonalPointRequest.setTradeType(PointsTradeType.EXPIRE_POINTS_EXCHANGE.getCode());
				}
			}
			queryPersonalPointRequest.setPlatformId(BaseContextHandler.getPlatformId());
			return personalAccountBiz.queryPointList(queryPersonalPointRequest);
		} catch (Exception e) {
			log.error("PersonalBaseExtController addPersonalPoints, request={}, Exception = {}", EntityUtils.beanToMap(request), e);
			return null;
		}

	}
}