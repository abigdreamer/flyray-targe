package me.flyray.crm.core.biz.platform;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.entity.PlatformAccoutConfig;
import me.flyray.crm.core.mapper.PlatformAccoutConfigMapper;
import me.flyray.crm.facade.request.PlatformAccoutConfigRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.flyray.common.biz.BaseBiz;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.util.EntityUtils;
import me.flyray.common.util.Query;

import lombok.extern.slf4j.Slf4j;

/**
 * 平台所支持账户配置
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Service
public class PlatformAccoutConfigBiz extends BaseBiz<PlatformAccoutConfigMapper, PlatformAccoutConfig> {
	
	/**
	 * 查询支持的账户类型列表
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午1:33:26
	 * @param platformAccoutConfigRequest
	 * @return
	 */
	public Map<String, Object> pageList(PlatformAccoutConfigRequest platformAccoutConfigRequest){
		log.info("查询支持的账户类型列表请求参数：{}",EntityUtils.beanToMap(platformAccoutConfigRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PlatformAccoutConfig reqAccConfig = new PlatformAccoutConfig();
		int page = platformAccoutConfigRequest.getPage();
		int limit = platformAccoutConfigRequest.getLimit();
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("page", page);
	    params.put("limit", limit);
	    Query query = new Query(params);
	    Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		
		BeanUtils.copyProperties(platformAccoutConfigRequest, reqAccConfig);
		List<PlatformAccoutConfig> list = mapper.select(reqAccConfig);
		TableResultResponse<PlatformAccoutConfig> table = new TableResultResponse<PlatformAccoutConfig>(result.getTotal(), list);
        respMap.put("body", table);
		respMap.put("code", BizResponseCode.OK.getCode());
        respMap.put("message", BizResponseCode.OK.getMessage());
		log.info("查询支持的账户类型列表 响应结果：{}",respMap);
		return respMap;
	}
	
	/**
	 * 添加支持的账户类型
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午1:40:56
	 * @param platformAccoutConfigRequest
	 * @return
	 */
	public Map<String, Object> addPlatformAccout(PlatformAccoutConfigRequest platformAccoutConfigRequest){
		log.info("添加支持的账户类型请求参数：{}",EntityUtils.beanToMap(platformAccoutConfigRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PlatformAccoutConfig reqAccConfig = new PlatformAccoutConfig();
		BeanUtils.copyProperties(platformAccoutConfigRequest, reqAccConfig);
		mapper.insert(reqAccConfig);
		respMap.put("code", BizResponseCode.OK.getCode());
        respMap.put("message", BizResponseCode.OK.getMessage());
		log.info("添加支持的账户类型 响应结果：{}",respMap);
		return respMap;
	}
	
}