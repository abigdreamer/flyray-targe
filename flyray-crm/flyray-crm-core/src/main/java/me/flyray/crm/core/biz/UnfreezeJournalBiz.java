package me.flyray.crm.core.biz;

import me.flyray.crm.core.entity.UnfreezeJournal;
import me.flyray.crm.core.mapper.UnfreezeJournalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;

/**
 * 解冻流水表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class UnfreezeJournalBiz extends BaseBiz<UnfreezeJournalMapper, UnfreezeJournal> {
	
	private static final Logger logger= LoggerFactory.getLogger(UnfreezeJournalBiz.class);
	
}