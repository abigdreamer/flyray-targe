package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import lombok.Data;
import me.flyray.common.entity.BaseEntity;


/**
 * 个人分销关系
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */

@Data
@Table(name = "personal_distribution_relation")
public class PersonalDistributionRelation extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //用户编号
    @Column(name = "personal_id")
    private String personalId;

	/**
	 * 分销级数
	 */
	@Column(name = "fx_level")
	private String fxLevel;

	/**
	 * 推荐客户编号
	 */
	@Column(name = "parent_id")
	private String parentId;

	/**
	 * 推荐客户名称
	 */
	@Column(name = "parent_name")
	private String parentName;

	    //创建时间
    @Column(name = "create_time")
    private Date createTime;

	/**
	 * 是否是直接推荐人
	 * 1是 0否
	 */
	@Column(name = "is_referrer")
	private String isReferrer;

	
}
