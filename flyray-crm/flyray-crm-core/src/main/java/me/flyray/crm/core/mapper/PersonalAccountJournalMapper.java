package me.flyray.crm.core.mapper;

import java.util.List;
import java.util.Map;

import io.swagger.models.auth.In;
import me.flyray.common.vo.QueryPersonalPointRequest;
import me.flyray.crm.core.entity.PersonalAccountJournal;
import me.flyray.crm.core.entity.PersonalPointJournal;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

/**
 * 个人账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalAccountJournalMapper extends Mapper<PersonalAccountJournal> {
	
	int queryPersonalAccountJournalCount(Map<String, Object> param);

	List<PersonalPointJournal> queryPersonalPointJournal(@Param("request") QueryPersonalPointRequest request, @Param("startRow")Integer startRow, @Param("limit")Integer limit);

	int countPersonalPointJournal(@Param("request") QueryPersonalPointRequest request);

}
