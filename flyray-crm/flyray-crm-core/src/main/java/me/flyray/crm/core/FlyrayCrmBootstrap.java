package me.flyray.crm.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import me.flyray.auth.client.EnableAceAuthClient;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * ${DESCRIPTION}
 *
 * @author bolei
 * @create 2018-4-2 20:30
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"me.flyray.auth.client.feign","me.flyray.crm.core.feignclient","me.flyray.crm.facade.feignclient.modules.admin"})
@EnableScheduling
@EnableAceAuthClient
@MapperScan("me.flyray.crm.core.mapper")
@EnableTransactionManagement
public class FlyrayCrmBootstrap {
    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(FlyrayCrmBootstrap.class, args);
    }
}
