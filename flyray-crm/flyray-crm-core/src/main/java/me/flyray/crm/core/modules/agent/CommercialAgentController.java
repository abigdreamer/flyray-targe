package me.flyray.crm.core.modules.agent;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.vo.admin.AdminUserResponse;
import me.flyray.crm.core.biz.agent.CommercialAgentBiz;
import me.flyray.crm.facade.request.CommercialAgentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Author: bolei
 * @date: 10:46 2019/2/14
 * @Description: 类描述
 */

@RestController
@RequestMapping("commercialAgent")
public class CommercialAgentController {

    @Autowired
    private CommercialAgentBiz commercialAgentBiz;

    /**
     * 添加代理商基础信息
     * @time 创建时间:2018年7月16日下午6:02:15
     * @param commercialAgentRequest
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse add(@RequestBody @Valid CommercialAgentRequest commercialAgentRequest){
        return commercialAgentBiz.add(commercialAgentRequest);
    }

    /**
     * 删除代理商基础信息
     * @time 创建时间:2018年7月16日下午6:02:15
     * @param userId
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse delete(String userId){
        return commercialAgentBiz.delete(userId);
    }

    /**
     * 查询代理商基础信息
     * @time 创建时间:2018年7月16日下午6:02:15
     * @param commercialAgentRequest
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<List<AdminUserResponse>> list(@RequestBody @Valid CommercialAgentRequest commercialAgentRequest){
        return commercialAgentBiz.list(commercialAgentRequest);
    }

    /**
     * 更新代理商基础信息
     * @time 创建时间:2018年7月16日下午6:02:15
     * @param commercialAgentRequest
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse update(@RequestBody @Valid CommercialAgentRequest commercialAgentRequest){
        return commercialAgentBiz.update(commercialAgentRequest);
    }

}
