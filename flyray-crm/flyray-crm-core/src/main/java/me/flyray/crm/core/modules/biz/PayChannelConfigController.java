package me.flyray.crm.core.modules.biz;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.crm.core.entity.PayChannelConfig;
import me.flyray.crm.facade.request.AddPayChannelConfigRequest;
import me.flyray.crm.facade.request.QueryPayChannelConfigRequest;
import me.flyray.crm.facade.request.UpdatePayChannelConfigRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.PayChannelConfigBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@Api(tags="平台、商户支付通道配置")
@Controller
@RequestMapping("payChannelConfig")
public class PayChannelConfigController extends BaseController<PayChannelConfigBiz, PayChannelConfig> {

	@Autowired 
	private PayChannelConfigBiz payChannelConfigBiz;
	/**
	 * 新增平台/商户支付通道配置
	 * */
	@ApiOperation("新增平台、商户支付通道配置")
	@RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> add(@RequestBody @Valid AddPayChannelConfigRequest addPayChannelConfigRequest) throws Exception {
		Map<String, Object> response = payChannelConfigBiz.add(addPayChannelConfigRequest);
		return response;
    }
	/**
	 * 修改平台/商户支付通道配置
	 * */
	@ApiOperation("修改平台、商户支付通道配置")
	@RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> update(@RequestBody @Valid UpdatePayChannelConfigRequest updatePayChannelConfigRequest) throws Exception {
		Map<String, Object> response = payChannelConfigBiz.update(updatePayChannelConfigRequest);
		return response;
    }
	
	/**
	 * 查询平台/商户支付通道配置列表
	 * */
	@ApiOperation("查询平台、商户支付通道配置列表")
	@RequestMapping(value = "/queryList",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryList(@RequestBody @Valid QueryPayChannelConfigRequest queryPayChannelConfigRequest) throws Exception {
		queryPayChannelConfigRequest.setPlatformId(setPlatformId(queryPayChannelConfigRequest.getPlatformId()));
		Map<String, Object> response = payChannelConfigBiz.queryList(queryPayChannelConfigRequest);
		return response;
    }
	
}