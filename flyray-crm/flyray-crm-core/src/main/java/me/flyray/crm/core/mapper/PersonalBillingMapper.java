package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.PersonalBilling;
import tk.mybatis.mapper.common.Mapper;

/**
 * 个人账单
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalBillingMapper extends Mapper<PersonalBilling> {
	
}
