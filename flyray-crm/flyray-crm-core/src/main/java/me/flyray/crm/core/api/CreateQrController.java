package me.flyray.crm.core.api;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.customer.CustomerBaseAuthBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 生成小程序二维码分享
 * @author Administrator
 *
 */
@Slf4j
@Api(tags="生成二维码")
@Controller
@RequestMapping("inviteQr")
public class CreateQrController  {
	
	@Autowired
    CustomerBaseAuthBiz authsBiz;
	
	/**
	 * 1，根据appid和secret获取access_token
	 * 2, 根据access_token获取二维码
	 * @param param
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	@ApiOperation("微信小程序获取邀请码")
	@RequestMapping(value = "/create",method = RequestMethod.POST)
    @ResponseBody
	public void createMiniProgramQr(HttpServletRequest request,HttpServletResponse response) throws ClientProtocolException, IOException {
		log.info("小程序生成二维码，请求参数。。。{}");
		//设置响应类型
//        response.setContentType("image/png");
        //执行post 获取数据流
		String customerId = request.getParameter("customerId");
		log.info("小程序生成二维码，请求参数。。。customerId{}"+customerId);
        byte[] result = authsBiz.doImgPost(customerId);
        //输出图片到页面
        PrintWriter out = response.getWriter();
        InputStream is = new ByteArrayInputStream(result);
        int a = is.read();
        while (a != -1) {
            out.print((char) a);
            a = is.read();
        }
        out.flush();
        out.close();
	}
}
