package me.flyray.crm.core.entity;

import lombok.Data;
import me.flyray.common.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 个人积分账户流水
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "personal_account_journal")
@Data
public class PersonalPointJournal extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String journalId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //个人信息编号
    @Column(name = "personal_id")
    private String personalId;
	
	    //账户编号
    @Column(name = "account_id")
    private String accountId;

	//账户编号
	@Column(name = "real_name")
	private String realName;

	//账户编号
	@Column(name = "phone")
	private String phone;
	
	    //来往标志  1：来账   2：往账
    @Column(name = "in_out_flag")
    private String inOutFlag;
	
	    //交易金额
    @Column(name = "trade_amt")
    private BigDecimal tradeAmt;
	
	    //交易类型  支付:01，退款:02，提现:03，充值:04, 发表说说领取：05, 分享领取: 06
    @Column(name = "trade_type")
    private String tradeType;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;

	/**
	 * 批量操作批次号
	 */
	@Column(name = "batch_no")
	private String batchNo;

	/**
	 * 批量操作批次号
	 */
	@Column(name = "remark")
	private String remark;



	/**
	 * 设置：流水号
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	/**
	 * 获取：流水号
	 */
	public String getJournalId() {
		return journalId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：个人信息编号
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：个人信息编号
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：账户编号
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * 获取：账户编号
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * 设置：来往标志  1：来账   2：往账
	 */
	public void setInOutFlag(String inOutFlag) {
		this.inOutFlag = inOutFlag;
	}
	/**
	 * 获取：来往标志  1：来账   2：往账
	 */
	public String getInOutFlag() {
		return inOutFlag;
	}
	/**
	 * 设置：交易金额
	 */
	public void setTradeAmt(BigDecimal tradeAmt) {
		this.tradeAmt = tradeAmt;
	}
	/**
	 * 获取：交易金额
	 */
	public BigDecimal getTradeAmt() {
		return tradeAmt;
	}
	/**
	 * 设置：交易类型  01：充值，02：提现，
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	/**
	 * 获取：交易类型  01：充值，02：提现，
	 */
	public String getTradeType() {
		return tradeType;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	
}
