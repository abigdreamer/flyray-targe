package me.flyray.crm.core.modules.customer;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.customer.CustomerMessageRelBiz;
import me.flyray.crm.core.entity.CustomerMessageRel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("customerMessageRel")
public class CustomerMessageRelController extends BaseController<CustomerMessageRelBiz, CustomerMessageRel> {

}