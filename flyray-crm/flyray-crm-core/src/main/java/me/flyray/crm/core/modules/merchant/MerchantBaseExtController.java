package me.flyray.crm.core.modules.merchant;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.merchant.MerchantBaseExtBiz;
import me.flyray.crm.core.entity.MerchantBaseExt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("merchantBaseExt")
public class MerchantBaseExtController extends BaseController<MerchantBaseExtBiz, MerchantBaseExt> {

}