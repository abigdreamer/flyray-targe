package me.flyray.crm.core.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.biz.PayChannelConfigBiz;
import me.flyray.crm.core.entity.PayChannelConfig;
import me.flyray.crm.core.mapper.PayChannelFeeConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("feign/payChannelConfig")
public class FeignPayChannelConfig {
	
	@Autowired
	private PayChannelConfigBiz payChannelConfigBiz;
	@Autowired
	private PayChannelFeeConfigMapper payChannelFeeConfigMapper;
	
	/**
	 * 查询商户/平台支付通道配置
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "query",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> query(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
    	try {
    		PayChannelConfig payChannelConfig = new PayChannelConfig();
    		payChannelConfig.setPlatformId((String)param.get("platformId"));
    		payChannelConfig.setMerchantId((String)param.get("merchantId"));
    		payChannelConfig.setPayChannelNo((String)param.get("payChannelNo"));
    		PayChannelConfig channelConfig = payChannelConfigBiz.getPayChannelConfig(payChannelConfig);
    		if(null != channelConfig){
    			result.put("payChannelConfigInfo", channelConfig);
    			result.put("code", BizResponseCode.OK.getCode());
    			result.put("message", BizResponseCode.OK.getMessage());
    			result.put("success", true);
    		}else{
    			result.put("code", BizResponseCode.PAYCHANNEL_CONFIGURATION_NOTEXIST.getCode());
    			result.put("message", BizResponseCode.PAYCHANNEL_CONFIGURATION_NOTEXIST.getMessage());
    			result.put("success", false);
    		}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			result.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			result.put("success", false);
		}
    	return result;
    }
	
	/**
	 * 根据第三方商户号查询支付通道配置配置
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "queryByOutMerNo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryByOutMerNo(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			PayChannelConfig payChannelConfig = new PayChannelConfig();
			payChannelConfig.setOutMerNo((String)param.get("outMerNo"));
			payChannelConfig.setOutMerAccount((String)param.get("outMerAccount"));
			List<PayChannelConfig> payChannelList = payChannelConfigBiz.selectList(payChannelConfig);
			if(payChannelList == null || payChannelList.size() == 0){
				result.put("code", BizResponseCode.PAYCHANNEL_CONFIGURATION_NOTEXIST.getCode());
    			result.put("message", BizResponseCode.PAYCHANNEL_CONFIGURATION_NOTEXIST.getMessage());
    			result.put("success", false);
			}else{
				result.put("payChannelConfigInfo", payChannelList.get(0));
    			result.put("code", BizResponseCode.OK.getCode());
    			result.put("message", BizResponseCode.OK.getMessage());
    			result.put("success", true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			result.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			result.put("success", false);
		}
		return result;
	}
	
	/**
	 * 查询商户通道费率
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "queryMerChannelFee",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryMerChannelFee(@RequestBody Map<String, Object> param){
		String platformId = (String)param.get("platformId");
		String merchantId = (String)param.get("merchantId");
		String payChannelNo = (String)param.get("payChannelNo");
		String payAmt = (String)param.get("payAmt");
		PayChannelConfig payChannelConfig = new PayChannelConfig();
		payChannelConfig.setPlatformId(platformId);
		payChannelConfig.setMerchantId(merchantId);
		payChannelConfig.setPayChannelNo(payChannelNo);
		Map<String, Object> channelConfig = payChannelConfigBiz.getPayChannelFee(payChannelConfig, payAmt);
		return channelConfig;
	}
}
