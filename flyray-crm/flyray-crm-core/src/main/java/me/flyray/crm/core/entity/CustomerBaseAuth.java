package me.flyray.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import me.flyray.common.entity.BaseEntity;


/**
 * 客户授权信息
 * @email ${email}
 * @date 2018-07-16 10:15:49
 * * 详情请参考  http://www.cnblogs.com/jiqing9006/p/5937733.html
 */

@Data
@Table(name = "customer_base_auth")
public class CustomerBaseAuth extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //用户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户
    @Column(name = "customer_type")
    private String customerType;
	
	    //授权方式  phone：手机号  email：邮箱   username：用户名  weixin：微信  qq：QQ  weibo：微博
    @Column(name = "auth_method")
    private String authMethod;
	
	    //识别码 第三方昵称
    @Column(name = "identifier")
    private String identifier;
	
	    //凭据 第三方唯一标识
    @Column(name = "credential")
    private String credential;

}
