package me.flyray.auth.client.interceptor;

import me.flyray.common.service.InputStreamReadRepeatableRequestWrapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @Author: bolei
 * @date: 17:46 2019/1/26
 * @Description: 运营人员查询参数处理
 */

//@Component
//@WebFilter(filterName = "operatorParamFilter", urlPatterns = "/**")
public class OperatorParamFilter implements Filter {

    @Value("${toplevel.platform.id}")
    private String topLevelPlatformId;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //判断平台编号是否是顶级平台,如果 判断token与platformId是否一致
        InputStreamReadRepeatableRequestWrapper request = new InputStreamReadRepeatableRequestWrapper((HttpServletRequest) servletRequest, topLevelPlatformId);
        filterChain.doFilter(request, servletResponse);
    }
    @Override
    public void destroy() {

    }

}
