/*
SQLyog v10.2 
MySQL - 5.7.20-log : Database - firstsnow_auth
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`firstsnow_auth` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `firstsnow_auth`;

/*Table structure for table `auth_client` */

DROP TABLE IF EXISTS `auth_client`;

CREATE TABLE `auth_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL COMMENT '服务编码',
  `secret` varchar(255) DEFAULT NULL COMMENT '服务密钥',
  `name` varchar(255) DEFAULT NULL COMMENT '服务名',
  `locked` char(1) DEFAULT NULL COMMENT '是否锁定',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  `crt_user` varchar(255) DEFAULT NULL COMMENT '创建人',
  `crt_name` varchar(255) DEFAULT NULL COMMENT '创建人姓名',
  `crt_host` varchar(255) DEFAULT NULL COMMENT '创建主机',
  `upd_time` datetime DEFAULT NULL COMMENT '更新时间',
  `upd_user` varchar(255) DEFAULT NULL COMMENT '更新人',
  `upd_name` varchar(255) DEFAULT NULL COMMENT '更新姓名',
  `upd_host` varchar(255) DEFAULT NULL COMMENT '更新主机',
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

/*Data for the table `auth_client` */

insert  into `auth_client`(`id`,`code`,`secret`,`name`,`locked`,`description`,`crt_time`,`crt_user`,`crt_name`,`crt_host`,`upd_time`,`upd_user`,`upd_name`,`upd_host`,`attr1`,`attr2`,`attr3`,`attr4`,`attr5`,`attr6`,`attr7`,`attr8`) values (1,'flyray-gate','123456','flyray-gate','0','',NULL,'','','','2017-07-07 21:51:32','1','管理员','0:0:0:0:0:0:0:1','','','','','','','',''),(3,'flyray-admin','123456','flyray-admin','0','',NULL,NULL,NULL,NULL,'2017-07-06 21:42:17','1','管理员','0:0:0:0:0:0:0:1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'flyray-auth','123456','flyray-auth','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'flyray-config','fXHsssa2','flyray-config','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,'flyray-demo-mybatis','bZf8yvj9','flyray-demo-mybatis','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,'flyray-template','bZf8yvj8','flyray-template','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,'flyray-biz','123456','flyray-biz','0','业务模块','2018-04-18 09:39:44','null','null','null','2018-04-18 09:39:44','null',NULL,'null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,'flyray-crm-core','123456','flyray-crm-core','0','用户框架','2018-05-17 10:03:16','null','null','null','2018-05-17 10:03:16','null',NULL,'null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,'flyray-pay-core','123456','flyray-pay-core','0','支付模块',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,'flyray-mall-api','123456','flyray-mall-api','0','flyray-mall-api','2018-08-23 17:00:36','1112','admin','127.0.0.1','2018-08-23 17:00:36','1112','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,'flyray-biz-thymeleaf','123456','flyray-biz-thymeleaf','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,'flyray-netty','123456','flyray-netty','0','flyray-netty','2018-10-30 19:09:45','1112',NULL,'127.0.0.1','2018-10-30 19:09:45','1112',NULL,'127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,'firstsnow-ssp','123456','firstsnow-ssp','0','ssp系统','2018-12-02 16:00:53',NULL,NULL,'127.0.0.1','2018-12-02 16:01:10',NULL,NULL,'127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'firstsnow-ssp-engine','123456','firstsnow-ssp-engine','0','ssp竞价','2018-12-28 19:28:52',NULL,NULL,'127.0.0.1','2018-12-28 19:28:52',NULL,NULL,'127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,'firstsnow-dsp-engine','123456','firstsnow-dsp-engine','0','dsp竞价响应','2018-12-28 19:31:08',NULL,NULL,'127.0.0.1','2018-12-28 19:31:08',NULL,NULL,'127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,'firstsnow-dsp','123456','firstsnow-dsp','0','dsp系统','2019-01-08 19:58:56',NULL,NULL,'127.0.0.1','2019-01-08 19:58:56',NULL,NULL,'127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `auth_client_service` */

DROP TABLE IF EXISTS `auth_client_service`;

CREATE TABLE `auth_client_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  `crt_user` varchar(255) DEFAULT NULL,
  `crt_name` varchar(255) DEFAULT NULL,
  `crt_host` varchar(255) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4;

/*Data for the table `auth_client_service` */

insert  into `auth_client_service`(`id`,`service_id`,`client_id`,`description`,`crt_time`,`crt_user`,`crt_name`,`crt_host`,`attr1`,`attr2`,`attr3`,`attr4`,`attr5`,`attr6`,`attr7`,`attr8`) values (21,'4','5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'3','6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50,'16','14',NULL,'2018-05-17 10:06:19','null','null','null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(53,'17','14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(54,'14','17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57,'16','17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(58,'1','-1',NULL,'2018-08-23 17:01:18','1112',NULL,'127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(59,'18','-1',NULL,'2018-08-23 17:01:18','1112',NULL,'127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(63,'3','14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(64,'17','19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(66,'16','19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(67,'20','17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(89,'16','3',NULL,'2018-12-02 16:26:56','1112','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(90,'6','3',NULL,'2018-12-02 16:26:56','1112','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(94,'6','23',NULL,'2018-12-28 19:29:41','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(100,'6','24',NULL,'2018-12-28 19:31:21','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(101,'3','1',NULL,'2018-12-28 19:31:34','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(102,'6','1',NULL,'2018-12-28 19:31:34','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(103,'18','1',NULL,'2018-12-28 19:31:34','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(104,'22','1',NULL,'2018-12-28 19:31:34','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(105,'23','1',NULL,'2018-12-28 19:31:34','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(106,'24','1',NULL,'2018-12-28 19:31:34','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(112,'6','25',NULL,'2019-01-08 20:00:42','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(113,'1','25',NULL,'2019-01-08 20:00:42','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(114,'16','25',NULL,'2019-01-08 20:00:42','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(115,'22','25',NULL,'2019-01-08 20:00:42','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(116,'3','25',NULL,'2019-01-08 20:00:42','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(117,'1','22',NULL,'2019-01-18 02:21:05','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(118,'3','22',NULL,'2019-01-18 02:21:06','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(119,'6','22',NULL,'2019-01-18 02:21:06','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(120,'25','22',NULL,'2019-01-18 02:21:06','17031762339246080','admin','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(121,'14','16',NULL,'2019-01-20 16:15:12','17031762339246080','admin','192.168.100.74',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(122,'17','16',NULL,'2019-01-20 16:15:12','17031762339246080','admin','192.168.100.74',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(123,'3','16',NULL,'2019-01-20 16:15:12','17031762339246080','admin','192.168.100.74',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(124,'6','16',NULL,'2019-01-20 16:15:12','17031762339246080','admin','192.168.100.74',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `customer_base` */

DROP TABLE IF EXISTS `customer_base`;

CREATE TABLE `customer_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(32) NOT NULL COMMENT '用户编号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_type` varchar(6) NOT NULL COMMENT '客户类型    CUST00：平台  （ CUST01：商户  CUST02：用户）：普通客户默认为空',
  `username` varchar(60) DEFAULT NULL COMMENT '登陆注册名称',
  `password` varchar(255) NOT NULL COMMENT '登陆密码',
  `pay_password` varchar(255) DEFAULT NULL COMMENT '支付密码',
  `password_error_count` int(11) DEFAULT NULL COMMENT '登录密码错误次数',
  `pay_password_error_count` int(11) DEFAULT NULL COMMENT '支付密码错误次数',
  `password_status` varchar(10) DEFAULT NULL COMMENT '登录密码状态  00：正常   01：未设置   02：锁定',
  `pay_password_status` varchar(10) DEFAULT NULL COMMENT '支付密码状态    00：正常   01：未设置   02：锁定',
  `personal_id` varchar(32) DEFAULT NULL COMMENT '个人客户编号',
  `merchant_id` varchar(32) DEFAULT NULL COMMENT '商户客户编号',
  `status` varchar(6) DEFAULT NULL COMMENT '账户状态 00：正常，01：客户冻结',
  `authentication_status` varchar(6) DEFAULT NULL COMMENT '认证状态（同步个人客户基础信息表中认证状态）  00：未认证  01：无需认证  02：认证成功  03：认证失败',
  `register_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `register_ip` varchar(46) DEFAULT NULL COMMENT '注册IP',
  `login_time` timestamp NULL DEFAULT NULL COMMENT '上次登录时间',
  `login_ip` varchar(46) DEFAULT NULL COMMENT '上次登录IP',
  `last_login_role` varchar(6) DEFAULT NULL COMMENT '最后登录角色（默认为空）  01：个人客户     02 ：商户客户',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `password_salt` varchar(20) DEFAULT NULL COMMENT '登录密码盐值',
  `pay_password_salt` varchar(20) DEFAULT NULL COMMENT '支付密码盐值',
  `role_type` varchar(2) DEFAULT NULL COMMENT '供业务拓展使用自定义',
  PRIMARY KEY (`id`,`customer_id`) USING BTREE,
  UNIQUE KEY `customer_base_customerid` (`customer_id`) USING BTREE,
  UNIQUE KEY `customer_base_merchantid` (`merchant_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户基础信息';

/*Data for the table `customer_base` */

/*Table structure for table `customer_base_auths` */

DROP TABLE IF EXISTS `customer_base_auths`;

CREATE TABLE `customer_base_auths` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_id` varchar(32) NOT NULL COMMENT '用户编号',
  `customer_type` varchar(6) DEFAULT NULL COMMENT '客户类型    CUST00：平台   CUST01：商户  CUST02：用户',
  `auth_type` varchar(6) NOT NULL COMMENT '授权类型  00：登录授权  01：支付授权（预留）',
  `auth_method` varchar(20) DEFAULT NULL COMMENT '授权方式  phone：手机号  email：邮箱   username：用户名  weixin：微信  qq：QQ  weibo：微博',
  `isThird` varchar(6) DEFAULT NULL COMMENT '是否第三方  00：站内    01：第三方',
  `identifier` varchar(32) DEFAULT NULL COMMENT '识别码',
  `credential` varchar(60) NOT NULL COMMENT '凭据',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户授权信息';

/*Data for the table `customer_base_auths` */

/*Table structure for table `customer_message_rel` */

DROP TABLE IF EXISTS `customer_message_rel`;

CREATE TABLE `customer_message_rel` (
  `id` varchar(32) NOT NULL COMMENT '序号',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '用户编号',
  `message_id` varchar(32) DEFAULT NULL COMMENT '消息编号',
  `isRead` varchar(2) DEFAULT NULL COMMENT '是否已读  00：未读  01：已读',
  `isDelete` varchar(2) DEFAULT NULL COMMENT '是否删除  00：正常  01：删除',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消息关联表';

/*Data for the table `customer_message_rel` */

/*Table structure for table `customer_push_msg` */

DROP TABLE IF EXISTS `customer_push_msg`;

CREATE TABLE `customer_push_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `customer_no` varchar(32) DEFAULT NULL COMMENT '个人或商户编号（客户类型为平台时字段为空）',
  `trade_type` varchar(6) DEFAULT NULL COMMENT '客户类型    CUST00：平台   CUST01：商户  CUST02：用户',
  `push_tag` varchar(40) DEFAULT NULL COMMENT '消息推送tag',
  `push_alias` varchar(40) DEFAULT NULL COMMENT '消息推送alias',
  `push_registration_id` varchar(40) DEFAULT NULL COMMENT '消息推送RegistrationID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户消息推送参数表';

/*Data for the table `customer_push_msg` */

/*Table structure for table `freeze_journal` */

DROP TABLE IF EXISTS `freeze_journal`;

CREATE TABLE `freeze_journal` (
  `journal_id` varchar(32) NOT NULL COMMENT '流水号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_type` varchar(6) NOT NULL COMMENT '客户类型    CUST00：平台   CUST01：商户  CUST02：用户',
  `customer_no` varchar(32) DEFAULT NULL COMMENT '个人或商户编号',
  `trade_type` varchar(2) NOT NULL COMMENT '交易类型（支付:01，退款:02，提现:03，充值:04）',
  `order_no` varchar(32) NOT NULL COMMENT '订单号',
  `freeze_balance` decimal(18,2) NOT NULL COMMENT '冻结金额',
  `status` varchar(6) NOT NULL COMMENT '冻结状态 1：已冻结  2：部分解冻  3：已解冻',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`journal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='冻结流水表';

/*Data for the table `freeze_journal` */

/*Table structure for table `gateway_api_define` */

DROP TABLE IF EXISTS `gateway_api_define`;

CREATE TABLE `gateway_api_define` (
  `id` varchar(150) DEFAULT NULL,
  `path` varchar(765) DEFAULT NULL,
  `service_id` varchar(150) DEFAULT NULL,
  `url` varchar(765) DEFAULT NULL,
  `retryable` tinyint(1) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `strip_prefix` int(11) DEFAULT NULL,
  `api_name` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `gateway_api_define` */

insert  into `gateway_api_define`(`id`,`path`,`service_id`,`url`,`retryable`,`enabled`,`strip_prefix`,`api_name`) values ('15','/payCore/**','flyray-pay-core',NULL,0,1,1,'paycoreapi'),('1','/admin/**','flyray-admin','',0,1,1,'back'),('10','/mall/**','flyray-mall-api',NULL,0,1,1,'mallapi'),('11','/doc/admin/**','flyray-admin',NULL,0,1,1,'admin-doc'),('12','/doc/auth/**','flyray-auth',NULL,0,1,1,'auth-car'),('2','/auth/**','flyray-auth','',0,1,1,'auth'),('3','/crm/**','flyray-crm-core',NULL,0,1,1,'crmapi'),('4','/biz/**','flyray-biz',NULL,0,1,1,'bizapi'),('5','/adminCrm/**','flyray-crm-core',NULL,0,1,1,'admincrmapi'),('7','/adminBiz/**','flyray-biz',NULL,0,1,1,'adminbizapi'),('8','/adminPayCore/**','flyray-pay-core',NULL,0,1,1,'adminpaycoreapi'),('9','/adminmall/**','flyray-mall-admin-api',NULL,0,1,1,'adminmallapi'),('14620496491524096','/adminAuth/**','flyray-auth',NULL,0,1,1,'adminAuthapi'),('13',' /doc/crm/**','flyray-crm-core',NULL,0,1,1,'crmcoreapi'),('14846502660812800','/adminthymeleaf/**','flyray-biz-thymeleaf',NULL,0,1,1,'adminthymeleafapi'),('14871194229354496','/thymeleaf/**','flyray-biz-thymeleaf',NULL,0,1,1,'thymeleaf'),('16278290698678272','/adminmerchants/**','flyray-biz-merchants',NULL,0,1,1,'adminmerchantsapi'),('16278308243714048','/merchants/**','flyray-biz-merchants',NULL,0,1,1,'merchantsapi');

/*Table structure for table `job_execution_log` */

DROP TABLE IF EXISTS `job_execution_log`;

CREATE TABLE `job_execution_log` (
  `id` varchar(40) NOT NULL,
  `job_name` varchar(100) NOT NULL,
  `task_id` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `sharding_item` int(11) NOT NULL,
  `execution_source` varchar(20) NOT NULL,
  `failure_cause` varchar(4000) DEFAULT NULL,
  `is_success` int(11) NOT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `complete_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `job_execution_log` */

/*Table structure for table `job_status_trace_log` */

DROP TABLE IF EXISTS `job_status_trace_log`;

CREATE TABLE `job_status_trace_log` (
  `id` varchar(40) NOT NULL,
  `job_name` varchar(100) NOT NULL,
  `original_task_id` varchar(255) NOT NULL,
  `task_id` varchar(255) NOT NULL,
  `slave_id` varchar(50) NOT NULL,
  `source` varchar(50) NOT NULL,
  `execution_type` varchar(20) NOT NULL,
  `sharding_item` varchar(100) NOT NULL,
  `state` varchar(20) NOT NULL,
  `message` text,
  `creation_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `TASK_ID_STATE_INDEX` (`task_id`(191),`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `job_status_trace_log` */

/*Table structure for table `merchant_account` */

DROP TABLE IF EXISTS `merchant_account`;

CREATE TABLE `merchant_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(32) NOT NULL COMMENT '账户编号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '客户编号',
  `merchant_id` varchar(32) DEFAULT NULL COMMENT '商户编号',
  `merchant_type` varchar(6) NOT NULL COMMENT '商户类型    CUST00：平台   CUST01：商户  ',
  `account_type` varchar(10) NOT NULL COMMENT '账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户 见枚举类AccountType',
  `ccy` varchar(6) DEFAULT NULL COMMENT '币种  CNY：人民币',
  `account_balance` decimal(18,2) DEFAULT NULL COMMENT '账户余额',
  `freeze_balance` decimal(18,2) DEFAULT NULL COMMENT '冻结金额',
  `check_sum` varchar(40) DEFAULT NULL COMMENT '校验码（余额加密值）',
  `status` varchar(2) DEFAULT NULL COMMENT '账户状态 00：正常，01：冻结',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户账户信息';

/*Data for the table `merchant_account` */

/*Table structure for table `merchant_account_journal` */

DROP TABLE IF EXISTS `merchant_account_journal`;

CREATE TABLE `merchant_account_journal` (
  `journal_id` varchar(32) NOT NULL COMMENT '流水号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '客户编号',
  `merchant_name` varchar(64) DEFAULT NULL COMMENT '商户名称',
  `merchant_id` varchar(32) DEFAULT NULL COMMENT '商户编号',
  `from_account_name` varchar(100) DEFAULT NULL COMMENT '来向账号名称',
  `from_account` varchar(32) DEFAULT NULL COMMENT '账户ID',
  `account_type` varchar(10) DEFAULT NULL COMMENT '账户类型    ACC001：余额账户',
  `order_no` varchar(32) DEFAULT NULL COMMENT '订单号',
  `in_out_flag` varchar(2) DEFAULT NULL COMMENT '来往标志  1：入账   2：出账 ',
  `trade_amt` decimal(18,2) DEFAULT NULL COMMENT '交易金额',
  `to_account` varchar(32) DEFAULT NULL COMMENT '去向账号',
  `to_account_name` varchar(100) DEFAULT NULL COMMENT '去向账号名称',
  `trade_type` varchar(2) DEFAULT NULL COMMENT '交易类型（支付:01，退款:02，提现:03，充值:04）',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`journal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户账户流水（充、转、提、退、冻结流水）';

/*Data for the table `merchant_account_journal` */

/*Table structure for table `merchant_base` */

DROP TABLE IF EXISTS `merchant_base`;

CREATE TABLE `merchant_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` varchar(32) NOT NULL COMMENT '商户编号',
  `merchant_name` varchar(60) DEFAULT NULL COMMENT '商户名称',
  `merchant_type` varchar(10) DEFAULT NULL COMMENT '商户类型：见枚举类型 merchantType',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '客户编号',
  `third_no` varchar(32) DEFAULT NULL COMMENT '第三方商户编号',
  `parent_mer_id` varchar(32) DEFAULT NULL COMMENT '父级商户编号',
  `merchant_level` varchar(4) DEFAULT NULL COMMENT '商户级别  00：普通商户  01：子商户 ',
  `company_name` varchar(100) DEFAULT NULL COMMENT '企业名称',
  `business_scope` text COMMENT '经营范围',
  `business_no` varchar(60) DEFAULT NULL COMMENT '工商注册号',
  `legal_person_name` varchar(32) DEFAULT NULL COMMENT '法人姓名',
  `legal_person_cred_type` varchar(6) DEFAULT NULL COMMENT '法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 ',
  `legal_person_cred_no` varchar(32) DEFAULT NULL COMMENT '法人证件号码',
  `business_licence` text COMMENT '营业执照',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `mobile` varchar(32) DEFAULT NULL COMMENT '企业座机',
  `fax` varchar(32) DEFAULT NULL COMMENT '企业传真',
  `http_address` text COMMENT '企业网址',
  `registered_capital` decimal(18,2) DEFAULT NULL COMMENT '注册资金',
  `company_address` text COMMENT '企业地址',
  `authentication_status` varchar(6) DEFAULT NULL COMMENT '认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败',
  `status` varchar(2) DEFAULT NULL COMMENT '账户状态 00：正常，01：冻结',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `owner` varchar(60) DEFAULT NULL COMMENT '归属人（存后台管理系统登录人员id）指谁发展的客户',
  PRIMARY KEY (`id`,`merchant_id`),
  UNIQUE KEY `Key_merchant_name` (`merchant_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户客户基础信息';

/*Data for the table `merchant_base` */

/*Table structure for table `merchant_base_ext` */

DROP TABLE IF EXISTS `merchant_base_ext`;

CREATE TABLE `merchant_base_ext` (
  `merchant_id` varchar(32) NOT NULL COMMENT '商户编号',
  `interview` int(11) DEFAULT NULL COMMENT '邀请面试次数',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  `zip_code` varchar(8) DEFAULT NULL COMMENT '邮编',
  `spare_address` varchar(32) DEFAULT NULL COMMENT '商户备用地址',
  `signing_date` date DEFAULT NULL COMMENT '签约日期',
  `release_date` date DEFAULT NULL COMMENT '解约日期',
  `wechat_no` varchar(32) DEFAULT NULL COMMENT '联系人微信号',
  `province` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '省',
  `city` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '市',
  `county` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '县',
  PRIMARY KEY (`merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户基础信息扩展表';

/*Data for the table `merchant_base_ext` */

/*Table structure for table `merchant_bind_card` */

DROP TABLE IF EXISTS `merchant_bind_card`;

CREATE TABLE `merchant_bind_card` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(60) COLLATE utf8_bin NOT NULL COMMENT '平台号',
  `merchant_id` varchar(60) COLLATE utf8_bin NOT NULL COMMENT '商户ID',
  `merchant_type` tinyint(2) NOT NULL COMMENT '0:个人 1：企业',
  `id_card_no` varchar(60) COLLATE utf8_bin DEFAULT NULL COMMENT '身份证号',
  `id_card_front_img` text COLLATE utf8_bin COMMENT '身份证正面照片',
  `id_card_back_img` text COLLATE utf8_bin COMMENT '身份证反面照片',
  `cardholder_name` varchar(60) COLLATE utf8_bin DEFAULT NULL COMMENT '持卡人姓名',
  `beneficiary_bank_name` varchar(60) COLLATE utf8_bin DEFAULT NULL COMMENT '收款银行名称',
  `beneficiary_bank_card_no` varchar(60) COLLATE utf8_bin DEFAULT NULL COMMENT '收款人卡号',
  `encrypt_bank_card_no` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT '加密银行卡号',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态  0：待验证  1：已绑定  2：解绑',
  `beneficiary_bank_branch_name` varchar(60) COLLATE utf8_bin DEFAULT NULL COMMENT '收款银行支行名称',
  `beneficiary_branch_no` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '支行编号',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `role_type` tinyint(4) DEFAULT NULL COMMENT '为了区分不同角色的商户',
  `account_permits_img` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT '开户许可证图片（merchant_type 为1时必输）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `t_merchant_bind_card_id` (`merchant_id`),
  UNIQUE KEY `t_beneficairy_bank_card_no` (`beneficiary_bank_card_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `merchant_bind_card` */

/*Table structure for table `message_info` */

DROP TABLE IF EXISTS `message_info`;

CREATE TABLE `message_info` (
  `message_id` varchar(32) NOT NULL COMMENT '消息编号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `message_type` varchar(2) DEFAULT NULL COMMENT '消息类型  00：系统消息  01：会员消息  02：商户消息',
  `message_title` varchar(32) DEFAULT NULL COMMENT '消息标题',
  `message_content` text COMMENT '消息内容',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息表';

/*Data for the table `message_info` */

/*Table structure for table `personal_account` */

DROP TABLE IF EXISTS `personal_account`;

CREATE TABLE `personal_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(32) NOT NULL COMMENT '账户编号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `personal_id` varchar(32) NOT NULL COMMENT '个人信息编号',
  `account_type` varchar(10) NOT NULL COMMENT '账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户 见枚举类AccountType',
  `ccy` varchar(6) DEFAULT NULL COMMENT '币种  CNY：人民币',
  `account_balance` decimal(18,2) DEFAULT NULL COMMENT '账户余额',
  `freeze_balance` decimal(18,2) DEFAULT NULL COMMENT '冻结金额',
  `check_sum` varchar(40) DEFAULT NULL COMMENT '校验码（余额加密值）',
  `status` varchar(2) DEFAULT NULL COMMENT '账户状态 00：正常，01：冻结',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人账户信息';

/*Data for the table `personal_account` */

/*Table structure for table `personal_account_journal` */

DROP TABLE IF EXISTS `personal_account_journal`;

CREATE TABLE `personal_account_journal` (
  `journal_id` varchar(32) NOT NULL COMMENT '流水号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `personal_id` varchar(32) NOT NULL COMMENT '个人信息编号',
  `account_id` varchar(32) NOT NULL COMMENT '账户编号',
  `account_type` varchar(10) NOT NULL COMMENT '账户类型    ACC001：余额账户',
  `order_no` varchar(32) DEFAULT NULL COMMENT '订单号',
  `in_out_flag` varchar(2) DEFAULT NULL COMMENT '来往标志  1：来账   2：往账',
  `trade_amt` decimal(18,2) DEFAULT NULL COMMENT '交易金额',
  `trade_type` varchar(2) DEFAULT NULL COMMENT '交易类型（支付:01，退款:02，提现:03，充值:04）',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`journal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人账户流水（充、转、提、退、冻结流水）';

/*Data for the table `personal_account_journal` */

/*Table structure for table `personal_base` */

DROP TABLE IF EXISTS `personal_base`;

CREATE TABLE `personal_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `personal_id` varchar(32) NOT NULL COMMENT '个人客户编号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_id` varchar(32) NOT NULL COMMENT '用户编号',
  `third_no` varchar(32) DEFAULT NULL COMMENT '第三方会员编号',
  `phone` varchar(11) DEFAULT NULL COMMENT '手机号',
  `avatar` text COMMENT '头像',
  `real_name` varchar(20) DEFAULT NULL COMMENT '用户名称',
  `id_card` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `nick_name` varchar(32) DEFAULT NULL COMMENT '用户昵称',
  `sex` varchar(1) DEFAULT NULL COMMENT '性别 1：男 2：女',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `address` varchar(256) DEFAULT NULL COMMENT '居住地',
  `id_positive` text COMMENT '身份证正面',
  `id_negative` text COMMENT '身份证反面',
  `authentication_status` varchar(6) DEFAULT NULL COMMENT '认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败',
  `status` varchar(2) DEFAULT NULL COMMENT '账户状态 00：正常，01：冻结',
  `remark` text COMMENT '备注',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `owner` bigint(20) DEFAULT NULL COMMENT '归属人（存后台管理系统登录人员id）指谁发展的客户',
  `personal_type` varchar(6) DEFAULT NULL COMMENT '个人客户类型  00：注册用户  01：运营添加用户',
  PRIMARY KEY (`id`,`customer_id`,`personal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人客户基础信息';

/*Data for the table `personal_base` */

/*Table structure for table `personal_base_ext` */

DROP TABLE IF EXISTS `personal_base_ext`;

CREATE TABLE `personal_base_ext` (
  `personal_id` varchar(32) NOT NULL COMMENT '个人客户编号',
  `personal_level` int(11) DEFAULT NULL COMMENT '客户等级',
  `industry` varchar(256) DEFAULT NULL COMMENT '行业',
  `industry_description` varchar(256) DEFAULT NULL COMMENT '行业描述',
  `job` varchar(256) DEFAULT NULL COMMENT '职务',
  `provinces` varchar(64) DEFAULT NULL COMMENT '所在省市',
  `email` varchar(32) DEFAULT NULL COMMENT '电子邮件',
  `zip_code` varchar(8) DEFAULT NULL COMMENT '邮编',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `attention_industry` text COMMENT '关注行业',
  `attention_jobs` text COMMENT '关注职位',
  `self_tag` text COMMENT '标签',
  `deviceId` varchar(60) DEFAULT NULL COMMENT '设备ID',
  `deviceType` varchar(1) DEFAULT NULL COMMENT '设备类型',
  PRIMARY KEY (`personal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人基础信息扩展表';

/*Data for the table `personal_base_ext` */

/*Table structure for table `personal_base_tracking` */

DROP TABLE IF EXISTS `personal_base_tracking`;

CREATE TABLE `personal_base_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `ticker_personal_id` varchar(32) NOT NULL COMMENT '跟踪人ID',
  `personal_id` varchar(32) NOT NULL COMMENT '被跟踪人ID',
  `remark` text COMMENT '备注',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人客户跟踪表';

/*Data for the table `personal_base_tracking` */

/*Table structure for table `personal_billing` */

DROP TABLE IF EXISTS `personal_billing`;

CREATE TABLE `personal_billing` (
  `bill_id` varchar(32) NOT NULL COMMENT '账单编号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `personal_id` varchar(32) DEFAULT NULL COMMENT '个人信息编号',
  `account_journal_id` varchar(32) DEFAULT NULL COMMENT '账务流水号',
  `order_no` varchar(32) DEFAULT NULL COMMENT '订单号',
  `bill_type` varchar(6) DEFAULT NULL COMMENT '账单类型    饮食，服装，美容，生活用品，保险，理财',
  `in_out_flag` varchar(4) DEFAULT NULL COMMENT '来往标志  1：入账   2：出账',
  `trade_amt` decimal(18,2) DEFAULT NULL COMMENT '交易金额',
  `seller_id` varchar(32) DEFAULT NULL COMMENT '资金来源编号',
  `seller_type` varchar(4) DEFAULT NULL COMMENT '客户类型    CUST00：平台   CUST01：商户  CUST02：用户   CUST03：第三方',
  `produce_info` varchar(256) DEFAULT NULL COMMENT '商品说明',
  `pay_way` varchar(6) DEFAULT NULL COMMENT '付款方式  ALIPAY：支付宝 \nWECHAT：微信',
  `remark` text COMMENT '备注',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `trade_type` varchar(30) DEFAULT NULL COMMENT '交易类型（支付:01，退款:02，提现:03，充值:04）',
  `status` varchar(10) DEFAULT NULL COMMENT '账单状态（付款成功 00 ，充值成功 01 ，退款申请成功02，退款成功03， 提现申请成功04 ，提现处理成功 05 ,退款失败06, 提现失败07 ）',
  PRIMARY KEY (`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人账单';

/*Data for the table `personal_billing` */

/*Table structure for table `personal_bind_card` */

DROP TABLE IF EXISTS `personal_bind_card`;

CREATE TABLE `personal_bind_card` (
  `id` varchar(32) NOT NULL COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `personal_id` varchar(32) DEFAULT NULL COMMENT '个人客户编号',
  `bind_card_no` varchar(32) DEFAULT NULL COMMENT '银行卡号  隐藏中间数位的银行卡号',
  `bind_encrypt_card_no` varchar(256) DEFAULT NULL COMMENT '加密银行卡号',
  `cardholder_name` varchar(32) DEFAULT NULL COMMENT '持卡人姓名',
  `bank_no` varchar(20) DEFAULT NULL COMMENT '银行编号',
  `bank_name` varchar(256) DEFAULT NULL COMMENT '银行名称',
  `subbranch_no` varchar(20) DEFAULT NULL COMMENT '支行编号',
  `subbranch_name` varchar(256) DEFAULT NULL COMMENT '支行信息',
  `status` varchar(2) DEFAULT NULL COMMENT '状态  00：待验证  01：绑定  02：解绑 ',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户绑卡表';

/*Data for the table `personal_bind_card` */

/*Table structure for table `personal_certification_info` */

DROP TABLE IF EXISTS `personal_certification_info`;

CREATE TABLE `personal_certification_info` (
  `id` varchar(32) NOT NULL,
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `personal_id` varchar(32) DEFAULT NULL COMMENT '个人客户编号',
  `bank_card_no` varchar(32) DEFAULT NULL COMMENT '银行卡号',
  `bind_encrypt_card_no` varchar(256) DEFAULT NULL COMMENT '加密的银行卡号',
  `cardholder_name` varchar(32) DEFAULT NULL COMMENT '持卡人',
  `bank_no` varchar(20) DEFAULT NULL COMMENT '银行编号',
  `bank_name` varchar(256) DEFAULT NULL COMMENT '银行名称',
  `subbranch_no` varchar(20) DEFAULT NULL COMMENT '支行编号',
  `subbranch_name` varchar(256) DEFAULT NULL COMMENT '支行信息',
  `auth_id` varchar(64) DEFAULT NULL COMMENT '第三方认证id',
  `auth_type` varchar(2) DEFAULT NULL COMMENT '实名认证类型 00代表银行卡四要素实名人 01代表泰华电子实名认证',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户实名认证信息表';

/*Data for the table `personal_certification_info` */

/*Table structure for table `personal_coupon_relation` */

DROP TABLE IF EXISTS `personal_coupon_relation`;

CREATE TABLE `personal_coupon_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `personal_id` varchar(32) DEFAULT NULL COMMENT '个人客户编号',
  `merchant_id` varchar(32) DEFAULT NULL COMMENT '商户客户编号',
  `coupon_serial_no` varchar(30) DEFAULT NULL COMMENT '红包流水号',
  `coupon_id` varchar(30) DEFAULT NULL COMMENT '红包ID',
  `use_status` varchar(2) DEFAULT NULL COMMENT '使用状态 00：未使用，01：使用中，02：已使用',
  `recieve_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '领取时间',
  `effective_status` varchar(2) DEFAULT NULL COMMENT '有效状态 00：有效 01：失效',
  `scene_id` varchar(255) DEFAULT NULL COMMENT '红包使用场景ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=430 DEFAULT CHARSET=utf8;

/*Data for the table `personal_coupon_relation` */

/*Table structure for table `personal_distribution_relation` */

DROP TABLE IF EXISTS `personal_distribution_relation`;

CREATE TABLE `personal_distribution_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `personal_id` varchar(32) NOT NULL COMMENT '用户编号',
  `parent_per_id` varchar(32) DEFAULT NULL COMMENT '父级编号',
  `parent_level` int(11) DEFAULT NULL COMMENT '父级级别',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人分销关系';

/*Data for the table `personal_distribution_relation` */

/*Table structure for table `platform_accout_config` */

DROP TABLE IF EXISTS `platform_accout_config`;

CREATE TABLE `platform_accout_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_type` varchar(6) NOT NULL COMMENT '客户类型    CUST00：平台   CUST01：商户  CUST02：用户',
  `account_type` varchar(6) NOT NULL COMMENT '账户类型  ACC001：余额账户，',
  `account_alias` varchar(32) DEFAULT NULL COMMENT '账户别名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='平台所支持账户配置';

/*Data for the table `platform_accout_config` */

insert  into `platform_accout_config`(`id`,`platform_id`,`customer_type`,`account_type`,`account_alias`) values (1,'14193960608149504','CUST01','ACC001','余额账户'),(2,'14644251517857792','CUST01','ACC001','余额账户'),(3,'14376521810391040','CUST01','ACC011','火钻账户'),(4,'14376521810391040','CUST01','ACC001','余额账户'),(5,'15806527374438400','CUST01','ACC001','余额账户'),(6,'15806527374438400','CUST01','ACC001','余额账户'),(7,'14376521810391040','CUST01','ACC001','余额账户'),(8,'14376521810391040','CUST01','ACC011','火钻账户'),(9,'17501260918894592','CUST01','ACC001','余额账户'),(10,'17501260918894592','CUST01','ACC006','总交易金额账户'),(11,'17501260918894592','CUST01','ACC005','已结算账户'),(12,'13608825924956160','CUST01','ACC001','余额账户'),(13,'13608825924956160','CUST01','ACC006','总交易金额账户');

/*Table structure for table `platform_base` */

DROP TABLE IF EXISTS `platform_base`;

CREATE TABLE `platform_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `platform_name` varchar(100) DEFAULT NULL COMMENT '平台名称',
  `platform_login_name` varchar(100) NOT NULL COMMENT '平台登录名称',
  `platform_level` varchar(2) DEFAULT '02' COMMENT '平台级别  01:一级平台（顶级） 02：二级平台（默认）',
  `platform_logo` mediumblob COMMENT '平台logo',
  `platform_introduction` text COMMENT '平台简介',
  `authentication_status` varchar(6) DEFAULT NULL COMMENT '认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败',
  `operator_id` int(11) DEFAULT NULL COMMENT '最后操作人编号',
  `operator_name` varchar(256) DEFAULT NULL COMMENT '最后操作人名称',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`,`platform_login_name`),
  UNIQUE KEY `Key_platform_name` (`platform_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='平台基础信息';

/*Data for the table `platform_base` */

insert  into `platform_base`(`id`,`platform_id`,`platform_name`,`platform_login_name`,`platform_level`,`platform_logo`,`platform_introduction`,`authentication_status`,`operator_id`,`operator_name`,`create_time`,`update_time`) values (1,'13608825924956160','flyray集团','admin','01','data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAgAElEQVR4Xu19CZwcVbX3ObX1MpOd7EgCJIBAAAkIAYMBo0AgREBQdhAEFUTwgcvn8qIPN1BQAk+JhIDgYxfwueFKIIKKYQ9+8oGTCVnJPpnuru6uqvv9bnVVde1bL1M9XfEnMJNa7z3/+p//Oeeei5D+ScQIHPaIOJMB5T3AYI9ElJdfObPnhUQ8WJc/BHb5+w/p6x/2aPlckof/qIzkDoUMwxIAMCaEAOEK8i62pLxU3S1ftPrs3NohfdguvXkKkCGY+MMeFj8tj2FvqI7ixoCOChM66KSoPyIAIQCsRGRuS+W/Xz4te/UQPG5X3zIFSJun/7BfVH4nTuTnA9bJwvwIOijUf2uMooOFG5C24A5y0itnCqn71aZ5SwHSpoGmtzn01+U14lhhmh8IDFS4PRcCYFVRsluqp7+4KPuLNj56194qBUgbpv6g1URg35b6yqO4KW7MQN0o8+/VRzL8LN3fqj8oykQRtlY/8vLCzGNtePyuvkUKkBZPPwUHs7bSVxklTHE4VR4gsGsQV1BRkGyrfuzVUzMPt/gVuvryKUBaOP1nPUTYf/VKa8TR3J7GQJtB4eFGUTcrFNMohHBbq+e8tjDzYAtfo6svnQKkRdOvMkd/pa88ss4coZhBA4frY9nApU6eTBR+e/XMVxdmHm/Rq3T1ZVOAtGD6VXD0VfrKo4Up+uX9hHlUDWLWKPS6FCTc1spJry3K/r4Fr9PVl0wB0uTpV8Hx70qfOFqYYmaMoOiU+e8jMY0WC8YqkYXN5eNfOSv3TJNfqasvlwKkidNvgGOUUItWRYxO2ZnBngdxvZ5BUQBYVmR+e+UDr56RW9HE1+rqS6UAadL0H/QQESBrilaFyJD7CfGwGsSccVfdraoiC9tSkDRpWt2zuc26eLdcxwCHSZA73l3zm0JFp8wne0W9fK4HFSJnB+UPvnwq/+dumYNWvWfKIA2OrAqOTC1aFUaIx2UGL3fLVduoGXciZ3aXT3xlYe6PDb5iV5+eAqSB6TfAMUIDh612qt0axA4WpEWOg5VTVp+afbKB1+zqU1OAxJx+mgR8nav0l0cJUz0dVZe8hV0zRGWGSNEu+m4ykfmdlVNWfzgFSZypTgESY9SmLyfZ/OjKW6pbZYoiuZWuG5dvowYxmEtPOtJk4s7KghQk0Sc7BUjEMauBo9pf7uUn6MwRJ29huW1UpjGtF/HSIC7ulsIPpCCJON1pFCvKgM1bTLh3ZklryyO5yUPNDNnBKvCiDFxZAVQoVQAQRJAyDMhC7f+VPGdUBWsZd5kblE98fSGfCveQE58ySMiB0sEhjuAmuzFGO6JTKCvQs70C+Z0VYGQCiAhEr2ykD8DoKxHrvy/nWKj0clDu4UDhGUCqSXbLH1y9KA0Bh5n6FCAhRkkFx8HS2+IIbtJQZMgpBvI7yjBiiwgMVfX0jwaOGkhq4Kj/vl4NrJYFUxgxCMWRPBTHCgAKyPxgCpIQU5+6WEGDRMGx+WBpbbnX6Va1Q4OwJRnGbCoCX5Y1UDiNvwYW+vtgRlEYgOJoAcQ8L/GifPzqhfzKoDHo5r9PGcRn9lVwHCS9Xe7lJllW+JnPaWF0qmeLCKO2lYHQe+gzZWcOr99bGEV3x+rXkQQGBkdniqxI5r96Bv9cN4PA791TgHiMjgqOA6W1Yi83OVSG3NKzR3d3rMtlA6Nd2uGsrMCY9QXIiopVY+iNHChI0LRwxPx7TZMY+RbNHVPVCaMzTU2rUOYR8+w2iWVPfPV0YVUKEucIpABxsQoDHD0aOMJkyPXrxKidMucthLIE49YVgFVU5aC5TVoPIN34aSmJBgpdY+hulivTBDAQAVgHHDv3r+fk1qQgsY5AChCbRdA8R6an2l/O8xNUfev2J2rewlT67pe3yJSqMJaCw2L8NXS6aowIjFIT8Kbr2BhIAViHbAoS+3SnADGPyJ8Jd8A70jqxh5vY7gw5X5bFPfoL1zNINgIolyPDfKjmBpmjU04tUXObXH5vCPe6O6X+l+qeWRlIP18BeBsZ5riUSepGkQLEBJD9HyhvLOeFmiAPcKsc6zAa0CBCWd5NdlXe/cZF+fX648xdsuNmYPBa9Wctv2F3p7zcLDWaFaBRvBiFUJBgChKzx9z1bqfqVmWr/eUefoIxGC2MTpkLFIWKXOB2sDNWX4Kb7BMx9/YdS4BhrvJmiKiM4sE0aP09IbAOkUk1iblXcreiRAVHptov9vATXDPkDTCDWarojGPWIHxFLvA73cGhz8ext+9YwmggqTGIe8bcwQjmaFYoBrJqFAXIOhRwzt/OzK/rVtswe7hdOQYqOAQNHF5ryM1c6wMW35WCLq18+GowOAyQ/GjHEgaZq2r5kDq1uYZy9cy67jz7apQARiGwBniY280g6VoNQsEhCLVold2tarUGoW4VP+DPHPYv1rE/2rUEGbjKEc3SQriBUa6Ami1PBiKwBrsYJF0JEAMcOa1kvYnMEJRxV8GxOxo4DCa5Y9cSRKxrklgZdm8GctU6lIEIWYNsdzJJ1wFkyh0k35uv9olansO+LLaZ0Sm7BqHgEAbjgcMAydJdSwDxKiNkq+8jojrMNH6rhXLdarO88ikuVcAORulSkHQVQCg4enqkteUsN64d3UUsGXIKjkJj4LCAhKEgsVX1RmWU0McbeZM1iNm5fzsTu0a4dw1AVHDkpbWiBo4wK/HC1k7putmrezsV5JkmgcMAyZ01JgnUHi75EF+hH8xAaxjoHpB0BUAs4LCr3wZrpxzlKLbr0VBuptgc5nAIdwoSNQTsUpulFSYGZuLN1cGhGYWsYUh3gGTYA0QFR05aK2a4ca2OTlmqfgmAQJmj1Bpw6GCZcxcV7ozmbgXUbGkhXyOfEqHGywWEaxhl+INkWANEBUe25lZFypC7bUEQkWmoIM+IrQVHHSS71RCwQ5OYM12OfEiIfEowo6xhhMzcvy0Yvppk+ALkDsLPyEjrxSw3vu0Z8qo8mBXZmW7lI63KyB61fOBGlsHr9XUenlEuDTQqIzTAKKYoVx9ymWOeP8VZKtOqd23ndYclQFTmyEhrSxktWuXWZb2JGXJzGoWryoVcuT3MYTeUo+/ZvYTRhHug9oiVYXdfh6IAvABThKNXHYHVdhpvO+41/ABCmYOXNohZbo/YGfIYfafovQRJHsyW28scTpAMfB+R+ZwKkACN4YhmedZs2da6u6wrIYC3rloofLYdRtvOewwrgFDmyPMmQd7GDHlGkguZytAwhwMk9xaWIFJNYk4a1teFeGbMPdeVBFcNEwSiMHDwiwsyr7fTgFt9r+EDEMocnLShlOH28FxDHpEZXPMgLhtsUubIVYeWOeyGctS9gzcySDWJ9sAhGCVMVxS/AklEuO8fp2YuaLXRtvP6wwIglDlynLRWFAIy5E2ITtlBI0hyISclgzlcQHITw+B1/l1RXKJZKpisvw+paXatWiCMAaTZxuHxp/MBcgfh92WlDSWhxhztzJALkrI7JzP7tTNaFdXsjrpv8NvI4BcdX/6ojBL++ENWnZJ5NepzJvX4jgaIyhystLZEmcM+whHzFvYkn7lWSw9pWhY7VeVCXkkmcziY5GeDNyHDXKdqj6Zk2H26qrDMqatO4n6VVIOP+lydC5A7CL8PI20QeW6P0BnyiBrEiASbF1MRgIys7M4pyWYOB0j+p/htZOCLll6+evcUJkTS0F4d7MEoCpJzXjw580BUQ0zq8R0JEMocGUZaW+aHIEMuyYU8LTy8svMSY0fdX7wJGbhOTyY2j1HqUS4Z8fiXTuafSqrBR32uzgMIZQ7QNIdJcrit+Y6UB3GJTtk1DdUc+SKzXyeCQx+Lox4ofhtUJnHp5avnN0Jk2J2apjaAZZ6f9Np83BzVEJN6fEcBRGUOYopW6UbdguiUXYPwslzooVW5HcgcDnfrweJNQKNbbp0bXdqTmvMpjmiWWdMQeHHVAuHwpBp7nOfqHIDcSjJ789J6GsqNxAzm/lYxNQjVHPlSZzOH3TiOfIhqEvyiHoBolFHodRTE/3jxZP7mOIaY1HM6AiAzbiUjZU7qFwVudNCa7zDdRSwJ9oBol0CZg1blDgPmcIDkEfEmRHJdDSQuHRq1fEiovlyAWzeP5vZadwyWkmrscZ4r+QC5lWSmc9JGkefGNCtD7hWdsoMrK8sD+TK7/3AEhz4GRz5a+hYifCm4CtjcttRZm6UQuOjFBcJP4xhhks9JNEAoc0iUOThudBhmsAx01DyIyf2ia8mzijxQBXbvdZfh9iRPYDOebfbPxZtY1KNb2hWjMcqyVScLlzXjWZJ2jcQCRAUHI/WX+Bo4YmfIQ0Sn7JomI8sD0iC797rPDX9w6O9+xM+L1wMy30UE+r/gbvL1Qsif4QT+kuFY6q56nklDrPo81K1ipY0ltuZWNZMZgjLkGUkekArdBQ7D3XqifAYQ8m1A2K++rZtLd/jaCZsB8XurFvDfS6QNNemhkgeQW0lmGiNtFFluTMsz5Ha3SpYHhEF23zeuw61NGt+OvMyRT1SPU0C+EBj8GAL0mBlFAfIQAHvvC6dwv+zIl4v40IkCCHWrqljTHHa3x6FBbMYdxAyu1zO5X1SQS8XuZI6INtNVhycGICo4QOovUUEeM0Ouz5xf4aFbxp1qDrmUgqOrLD/kyyYCICo4iAYOW2FgqzVIRknBEdJWmnoYufDyvcqcdBgwzHRA5BCgX5Ckv+Py5W839UYNXmzIAULBUVHc3Sp7CCEqM9jdKvv1qFsll1PmaNCGQp9OLrigp8Jw1xBkLwUke9Pt49RIkRFSBkIA/4gs3C0o+BAuXTrkTSCGFCAGONgYGfIGNQjNc6TgCG3bDR9YvfDjcySAnzMMTqqXtdDWQ9aQcm37OBU02wCUSzN33PlEwzdv4AJDBhAVHLLUX2K1JKBbviJm7ZShYVw2xaGTk6PgqKTM0YDdRDpVvODihaDgI8CioFFG7XxtTYm50yP9XY1R6F8TQhCuzC6980eRbtjEg4cEIAY4GJcMeYPM4FmOooElR1JwNNF+Ai9VPu/iWYTg88BgxgCHqaG24V7Zqoj1lY+16CR+Jrt06W2BN2vBAW0HiAqOqtRf1JgjTIY8bnTKrkGyRB5QqilztMCOXC9JFl6eF3urryPCNDdwGI0hNC1ibhRRZxJtMRYODUjaChADHBpzWEa1wdqpoDwI1RyKlIKjXeCg9yl99KIbEJkvq/dktDubNiI1u1vq/BnulQ4Ks4BX3a7rsz/5SVsz920DCAVHuSr1l1AT5KaZanZ0yq5BqFulyCk42gkOyh7lbHkrQczpPYANbeGhPSxaRAWVZp76v2puWFtB0haA7HkzGYuK9FaJaX+GnApyRUnB0U5w0HuJZ110GgF4goLC072ybO6jfdaM0K8W3bJrk1qUq20gaTlAauCQ+0rIjrQsdnJrC2qIDffq3aDolL3ql7pVhKTgaDc4VICceeFNgHRZr+Ymqe6TLaRr1x4eWsRVmxDl+uyyZS13t1oKkEBw6CAJAZag6JRdg1BBnoJjKKBRu2fp9IvuBwY+ZgaFqxbxdbfq3VIcbhqDoBDlG7lly/6zlW/ZMoDo4CgCOzJwJaBbvsMOHp9G1PYMOdUchC526qL1HK00kjjXLi668AmGxdPMy3XdtEhdmGvdTlXAWLWHXYvUdvPVF2uQ72WXLbs+zjOGOaclADGYAyK4VW7uVRiQ2N4yByk4wkx8q48RF134E4JwWS0zbmIC08/1KBYxCXKrFjF3UamFfk1umvFz60DSdICo4JDlviKyIx1VuS5VuuatkhtlmizIA4Apc7Ta+MNcv3ja+Z9DZL5f/9KbMuT28hJDiNcbR9TO0yzIYAzTz0aUy+j99L3s8uYzSVMBYoAjolulg8RIGvppEvvsaGOmMkcKjjC225ZjyovOO1AhzGojtGsBhb0Gy8O9soPEBIr6dU3dWAi5Lbt82Wea+YJNAwgFB0i1aJWDOVxK2B3M4da/yq3sxNYsjmIpTzUH0/nMIV599UwWYLwMsCV7663/r5kTPRTXKi684AUEeE9NQ9S7+zndLnOS0ORGmUK8NYFv0yYWLWIwTlNB0hSAqMwhBbhVTQSJmWlU5uhAcFQ/9an5MsGzEfEoBWEqgzjO3LyNGpFCyDYGYR0gvMAA8xB/662/HQpDj3vP8qnnnU6Q+Xk91OuuRSzv7Rn61Z7CFRSOquCmgaRhgKjMUZH7Sgw70hhIP/FhGu1GmUZlDrZzmKP6iU8fIyO5iACcgYhGV3o9alPP81ibuJl2fqJr5R9lGeV/+B/e/nRcw23XeZQzxFPO+z0g8wGzFjGiVFr5ibNg0fT+HuUnZkayM1RN2JOfZO++6/JG37UhgLiBwzWZ5+IWNSrgOwkclUs/+R4F4WYAnOdIluml3fTfdLmQ8bM5qeZMsgHiHxhCviTcdts/GjWCVp5PTjxrbIkTXkVkptD7WNd/aO9lLnG31GTVI74OENjctlro1+bGgdIwSGIDxACHV4Y8jNAOyTT2CLAKDi75zCFeeukMULhvA5Az1am2x/fNURrVemxRGn12tPMcDANACOIjAOQr2dtvf6OVht7ItcWTzt9fYcgKRGai2YidRu1RXuJebmIMpzUvYhrHGoh+krn7riv0rVCivkcsgIQGhw9I4jJNp4CjdOHllyGQ24FhBEvXOw8QuDOHzWf3YhgCFYYhVwq3335nVANo1/HiKefuoxB8VgdJ/X01d8oe6vUM/fprEXvVMGUshZCfZu+56+I4IIkMEB0c9gy570AH+lMB02QK5ULCmYOcdZZQzoy6nQBeZi2zMO3R4AYSo92KqZ1fRIYBAndmtr1zJT78cKVdhh/lPnWQ4ET1PNfS9/quvPW16sYyXPDNzNtL6tUwsdGYOxZIIgHEYI44GXK7n+QGGpfR1g/L02hV0sFx/uWTRUX5OQIcHeQuWTLE5lVjDTMM/FVQpDNw6dKNUYy3Xcd6gcS6HsRDm6gaxpRMNEK/to+PR5UwgehMEhogKjjKjWfIQ02EDTy0tgqEZGsOcvnlvLhL/jMgHOsqxNU5tEen6kLcEpVpmGHgxUw+eyzecksityIQ55+7j8Lj04g41WASi1FrVhKu9N1WflJzS+sMZS1jIYQ8kL33rnPDuluhAGKAw5Yhj+1WhUGJ7lZ1ADjo64hnX/JjAniFFRymKIxe62+fPFfh7uJmGX1Y3YW8U8PAg5mlP/pYmKEeimNKC86dRgj+xQISyqSOVkAu7pWtJkt9/oAVi+YGEVFAEggQFRyiB3M0Mflnz6zTd+4E5lDBccbF1xCEWxzCM2TIVj2vBQxD9/3I3Pnj7wwFAMLcsw4SmKq1kzc0g8GomoawZOJNbpYq3ew/u4WKjevUPj4EIBST+AJk+uLBSZKQ/Zdv4WEIkMRhGpohT7pbRd+r9OGLpwOSNxGRtWfC3X925jc0f8BSjtEUDUOIjIS8J3P30lfDGOxQHENBAgSfBoS9nCBxLztxB0W9atjdbdOY11SuooJE3H0+Pvyw7PXungCh4KjwmTdF5HqMk03s3rRaKpd+WDSUC5lkaw59TEqLLnwAED9amxTttza3iW64YYm++GgR8+6xFs0Sn2GeyC6/48NDYfxh70lOOGuqyPPPAoN71bWDaTwDarDqzeacKxZ9y1hqWu+xTHnwLC+QuALEDA5HsMmPMfwKDkMUHtLByRJ5F2bYfTphsVPxlPOPYVjmL4FMYf4IOMDhLdzNkd8a+gJCxSpIdYFaByurkLn8T5euDGuwQ3GcN0hM7+1admJjZJeqYQfo7G4bIY9lKu4gcQDEAAdw6r4QjhL0CBlyy/oOv86J2m3yRNrJ5bi911yLO4dikqLes3TqBU8BwPvrDBvPiFvNMAC4Mnfv0rlR36/dx9dBYnK3dNC7aBFHlXAILeJYwagKfPWr/1hmfd/Z+NRTkvm9HQCZcoO0S3QpH4lq7K7H+4AkT6Qd6/4PP7bdkxL3fuKC808iAL+pf0ScX261J6C51srys+nL1waGQVBOz9y37PG479uu81SQCDzVJPsYmsTMjPbQr8+KxchVwtTdmjntI7h4saK/rwUg075ZXr2bCAd6brVsHyU/honANHmQdmZ7uWlvXo0D7ZqIRu9TOumcHwMyV2jhp9rlvEK2xmgPIcMo5J7c/csubvS923E++cA5E0UWqCbZxzquLh8hlVnq7qT6X41VCd+Y/dnyLzgAMvOb4o3bSOb60K15AsASdvlsHjqLOehr0w9+6cRz1iPA5DooTOLMDhafMpJ2MQwhsCH7wLI9wybI2gEEv3sEg8T6sXFUCdu1iG/o16TtKEMpymnZ++/+X3Ua6T9o18Ndu5TtMjK1UKXHprKxNYkZjqYb5EDamRvRWcxBX6U4/6NHA8JzDuO2JPM0RjHWEw89w7DAzBEevPOvQ238Ye9PQVJi4VlkoMYkbuOrMoYLM1tK34NXLNZDx+oncH02AzPw7rtFFSD73iA+vgMyi1wbSUdxq8K8ufYuOZR2rO8gzWF+NXH+2d8hiF8wokoWhrB9XsyZcy+wtIlhAOC72Yfu+mKYaUrKMSqTcLACEPb3BYm2HsTy0bJ3U6Ea0KVq2Iw9vXQeAb6QuX/5jSpApvxXdaAE3AhjakMk/xphmhyRduZGdh5z6EZT/MBHHgfARUZZiZrnMHXtMMbP/HtS24Dc4n75uGXm4wwGbkzDAOKK3CPL5yXF+MM+B5l3zh4lQVmJyNRAYinX8WFmS/lJlDIWWtmgvJZ78J5ZuPcNpWk7SXaNm942kBWjP5WXO0aZA6vcjHWLcXvYAUraccXjz3gJGebQepTF5MO61UwlhWEI/Cv32N0HJG08wzwPBYkoKCshIkgaqRLOAE7FGd8sfXy7kl0Wqz9ViOSfGVs8KDIvlaevX5xfF2ZQknpM8fjTNwHgxFr+wrYc1sh4m36fHIbZlXvsnvoW20kdYI/nIiefNb4osa8xDEzQGds1FGxmmAaqhBmA03Hmf5Vu2EqyX46at3AI+RBJxVFYXdH3ZaHjKN48X2TePK5ERpcQkTPcKhsoDDfAYI7kMEx2F5PDp+4WOwwbxuMWP/TR6xHgRmv00M/tqhmmm/ZwVEDYq4SRXIUzvlH6+jbIfs2yZscrgd5gmclorK7s+4qQ+Iyun/GQIxdMEnPCRkdtlSPZR2MuCWSYamXv3G8fWNOxAJn/kTMQ2Uetms9F2+lahbpGUbrGm5O5DH4e9/9G6fx3SPbesHkLX6YJKCfhQJF75co+/16cW9upE0Rmz+ZLuakFZJA33FKzQA8BiqFkmGyB7WgGET941vcJwc95C3WfQIhRrhJuxSIBcgFOXVzcs4g5983bA9wmz7/2YZoskXYyCrdvJ4v0wrELNyGCpkHsjRXcVg7q6z1shXUhwFTTcE2KkgHsyv3mZx2rQSrHn3mUhLgSGabu3trLeTR3N1S0y14lbPuZQ3x/Lcz7DWmnSNhRrsGquG6Vz3kZIg+wdNenDo1kFY455SUG8VALzRsBC5Mxx3S7WsUwRFFezj95/2GdyN6VE844XAbmT4A4ymn89ipn1b+qvaZnSNgUMnerEgYoZviS2kYX9vmG+NgOJfPhZpWZ2FPxbkyThc5lkuKcBbTob1FQmYlrla7ruo62McyTuSfvP6nTAFKZe8bhVQ7+xCDWPuKWAlA3hq1/pFzBZAaP4XZZy00IKI/mn7jvIypApi8mowuobJUIwwaVk8Rxq7zSKBmUB1i6uWaHMUnh6JMWM8j8pyrUHe6PvUOiNlkJYBgk8N3s7x/oqEy6Cg6WggMMcLgyQyjGMDWVsx9vqxJmFJiV+dW9r+l1kLDv4uKS7ZC7yvfr4qdJIlTvmkFI67E6TZMUjzr5aADynLPUPVqZSbsZBgDn5P/wYMfUYlXmLjy8yrBO5nD7KBnMYl9iYGWYUNoE4L7cr+67wOSo1WCx19cr/9xNeP9Ma0iQRGGaDHSWJlGreY/80DpAnOK+LNYk1BPCMARhY+4Pj0ztlGre8nGLZkkAz4Rxq5qnSdQr9WeL5YPxqYcHHQChv5i0uLqrTLiRRoZYd818i69svBMSROazcijvZBS2Y6JbpSM++GOCeEVtcvQ3sQtD8+aLPvRujp2bhGUzGUYhcE/Pnx/tiPUg5TmLZkmssoJhmDGeUTw3LaJrE1+GsdbMaUpeF/RFhihzMr+9/xXbjNZNlS65LUPmTZFozRpCZMhju2WmE+ltVE3SIdEt8fAPnKQw+BtrtMQSttI+bOYdkOwCs30aBoCck3/qsQeSLtDLcxbMklhmBYMaODyN3t4dxvZxCq1JtBFBKBGZzM//7oFnzWNkfPvMv6QgEZXMm2XkeizJsBAZ9ka2UaPvlO0gkBQPP+EpAHy/60pCtxJ2x6S1iWEAns2tePzYjgAHgysQmDHO5cou0SvL1hFef++iSSx9yNRPXIkAOMDh6mLpg2gGSTvXiXQSSEqHznsfYZlnDB84EBRDwzCg4PG5lY/TBhOJ/aMyB+IKBrHmVjn2S7EnS11+ttTEmRnFJU+ir1Wg4EB0BYcvQOhfqiAh2X+VCevQJLGqf83LHwI0TZbpDHercNi8xxFxkdWX1Wnb2LPY8temZrLW37eAYRRQnuxZ+ctE5z5UcBC6fwiOMT42Ji1m7MDlyRhOd8uRL7GBRzO/ElGY+fk/Wd2qQBfLfMCei8lYich9Bkh8StwbZhpbqLgTQFKeddwsmWFeBATWMrmGsQeAJWD9SKNRMpaQWZlnf/VaUqmjPGf+LIlwNeZwM2J7UjAGSFwZnjIHIfPzf3rEojns4+SqQewHqSBR5L4ysCNDaZKI60QccQAT02QYeYBLuHAvHjL3GgJo7c1ry5gPBcMgwJdyz/06sb15y7Pnz5JYNpA5aoxbd5McmXQ3EHmCTQ2M1DRHADgCXSwHkzQJJFGZpiNAMuu47wCC0S7GuV7BxCS1T1oMtyu8hgGEH+af+8Emt18AABDXSURBVM01iWWO2R86QGLgWd2tCmf0HoLbK+Tr6lZhiWA4cEQCCD1YZxKRmHa0tTXviNSzNwLTZNgOYJKD595LgJzvXivkX/XbZIa5L/e3315EG9gkESBlCg4gTyOD4+tuqU1IO5YpexceWrvj2zc8NUW3KHMo8vz800/4ulWRNIinu6UL9zBGHqLtqNs6EzvTJB0kBIAtHfS+bwHC5+1uQX3HSRuT1Ds/aBjRvd54DEMA7sw/P/qTCN4dy4cSNOXZ8w6QCGcDRz2Javm4RNUklqUBjqUFJUKUSOCIzCD6wFqYJEKGvZHFVvoHJekgoWNUOvDY8xRQ7kJkBGc0xatWqEGGIVBBVK7MrfpTYjfyVMGh4NPIMCbmMDFD6OSee82bp5umag4SGRyxAWJ2t9Tols4QMZki6vmdAJLiAXOOIYgPI4NT9P3PLVEuj/IS4+tu6afl0l7TpGEIwDYE5dT8C08lthCxPGveARJLwWF2q1zcJhtIvLWJ3lc9ID9CwUE1RwS3qiEXy024i4pVk9QnOV53+DBMk+GSr0nIQXPGFmXydWTwk4QA59aJsTGGAYkAWZpHZTG++MyWoXSd/O4tHnL8/hKjPMOgjTl8GSPuIqg66JBomuO5X4XWHLHCvH4vb7hbHiBppFuKvWDSzjQZThngFCbx60nK+80+QGb5bxHaRsanujcKwxDAP7AoX5l9eeUbSQUGfS5y0LzeEkteAgb3dbyfJZrntgLQCRLX8XNfRFUiijI/3wA4GnKxHEwiy31qdKuJTebqVbLuJkCHVOCUAZ4kHyT0DYozjz4KGOVMAsxJgDArahIQEF4jhPyWJfhEbvXKRG+Io89Y4ZC5jyAyZ3pV5bqCRncvQ2sS/W5GgIPu7tswOJoGEEOTNACSRpimk0CiT2VxvzlTCUgLEJmDCeKeiLAnIbAnItUssAEZWEcIrEPEdQSUf+ZRfhxXP78pyWxhf7bSrOOOIwgrLCBwK6dRLdFjDbkHSJzaxNAkJQLYFHA0FSA6SKqy3FeOo0lirkjUGSvDKwN8B7hbnWTgjT5r8eD3PUUA3h8mCVi7l1+jBZ+Cwzq4SiDL8/PP/y625mi6BrFfUNUklEm8QGJmwwbcMTfGUZlkBLN3J+xv2KjxJf18ss/sUaVcdofa0tCxt2JAtxHPJKE3SBBhkCjkxGaCo+kMok8aBYnOJEFVv74THWOxlsokKUiGHD+Fdx9zCjL4S9cuJCEKDj21iSt4YJAQ+YSe5//4fLNfPFSxYpybWpgkDlM0wDQpSOLMWHPPKRw45woA/DF1r3yFuFFJ4FI54KVNzCABGCSotAQcLWMQC5NINU3ihZGgUG6oRL0L06Qgaa7BR71a8d1HXwvI3Bwqz+NT0u7dvZ1eGQaJDCf0vNR85jB/n6O+e6TjDXdLdoLEcqEYjR6Czk9BEmmqmnpwaf+jz1MYvM9NoLtvfOrDNG5uFQWH0lpwtJxBzEwiy9JbJYkbHZUxLAwSYecr/d4ZIdUkTbX8kBcrzJh9OPL8qmYvgtJuP0iIckLPSyuarjnsr9cyDWK/0YzFZGRBlvrjgCSIKYIaRaQgCWnVTT5scP/3vsMwzHgvkPhqE7f1MrXnGyRA2gKOtjGIPu5jF5ORGUnqL8kak/iUajWbaQRBGRDS6FaTIeB/ueJ+772ZIF7bcMFhvfXPIMH2gaPtAKE3tICk0epfLbXkm2M0aZuUSdqKDyhMmz0Zs+waAiiEAUkAo9QE+erWu1XmUWqbi2W+KQUJL0v9YpWr7VXhIdCbsqmP7fJCJmWSdsJkcOaR1zEM3mSJZoXufKiVshMcJATbDo4hYRCzu8XLcn+5ygYL9yYzTSaTCve2gmS/I25mkLk2KkjoMxKAASDK/J7Vz7ZckLuNyZAwiAUkktwvSuzoqI0czMI8DtOkTNJOiAAMzjjySwyDtOTfub+HkQexLoICwE0cwPzM639Z3d6nrd9tSAGiaxLeBBLzEu1mJRe9QsUpSNprdoMzj/ggAvNZAHIyMgzjDhba4YcQRLwfJPh8/o3n1rf3Ka13G3KAGCCR5X6xwlr3z4tRi2V5Pdv5bkwjZFNN0m4DLO536FRFES5gWDiNAEynvXhpaT8gvEEIPMMr8IvMm397vd3PlTgXyyncw4OkmUwjZMmAMALTKuAkWGTCniERDKKPyfRbyGhxu/JWqcKMDUr++TGFfY/EwDFHACrcsYeZufl6fCfw+C46YOo9ZA4qcDBB+Pf6i/GPXfTqRoA1ce886avStlKFbR5IQkbBhCzZJYzAfbp5PcnIO8nY3grcLRfgfeVBGI0KoFnD8b1Q4HrhRRgBl2+8EP+ZOONp8gMlikHM7zbxq9I2scyOtQtszzXvDa5INFYm5skuksUDtnweO2p5azPsYuod5ObSJvisUgUmiKGRAZKbBP+74VN6Z/tmPEHyrpFYgNSSiXK/WLYJd30M4wr4gKphqm34rFKAPDOjm0Ay+b/Jr0sbgG6T4GoTXj0DsuOhb9NkOADOxkryzLvxJ0osQPToFifJ/eUKOzpScznzxk1xFmshgNBFIJl8K/l7aTMcGbQFuJe5ZcbBms3X4t6Nm2PyrpBogNDhonu4l2TlLbEcUbg3gWmErDIIeWbmcGaSybeRv5Q2wjGGacZcl5PfE+7e8Em8JHkm3tgTJR4gFiYpa2Up9ipgv3Ui3qVe1m3OPZhGyA1fd4syR3ETHOnqPnn5WnRCXEBENYnwLth38yewrzGTTNbZHQEQfcgmflXeKorMOFdb9tverUEBL+TJAORw/+HEJJOWkGfFjTAnrlvldl5uAjy/8Wp8b7JMvLGn6SiATFlM8rIkrxXL7LigbilBIIp6Pp9XCjhMhPvkH5qYIyTDBo0XNUMmA9L2ryHfmEkm6+yOAkidSaQtosjuETRpQSCJ2iZV6Ol8Jpl0K3mutBGOboZb5VZg2jsDjl93CSZ6R90oEOxIgKhMIstrSyV2nPllHa5xXG3icx4NAWNvZ4aADeYIsd993I9Pfi/4woYr8MYoRpjkYzsSIBYmKbF7tKTq1ydUrGqS8bj/lis7J5moMscGjTn83KoIjTHcpF3vu+C2dZ/CzyTZ6KM8W0cDRNckJZEdFyXD3gym4XNKAScyMzoBJJN+QP5e2gRHOgwjRLVzkJtqZ5rcNPjyxivwW1GMMMnHdjRATNGtd8SS3j2jvmNwpORiDHeM7yEDmHAmmfwD8nxxExzhmd6I8d5+rZhGzoST3v44Pplko4/ybMMCIDUmUdaWSoxFkwR9MV1DnKaTwjBNkplk4i3kZXETHOIbym0gBG5fcsBmQN6+GLkoBpj0Y4cFQAwm+Yr8TqmkbfNlG/kwxm4wTkQfnc+T3TgB90uSuzXpFvKP0iaYbQxDiBq0RrVcdiK8sOkarN8z6dYf4vmGFUAMJilqTBK3oFEfuJDn08P4PCngREyEJlGZY6MHc/gZRcwyE/WSCKR3Pzh8/SX4Ugi765hDhhVAzJqkVNA3jIy5kWiY/d9tTMP3DD2TqMyxEWbHzpAHfBy8VnLmJ8HzG68ZXln0Gu6H4R/KJJKkrBUpkzTgY4ceHdOXVxhCJpl4M3m5tBEOibIcOWqy1GIu2nvzI6AojIfxG67A4nAzp2EJEBOTbCoVmIkNg8QHZAY2TNEgvhcGcDy0NU+iMscGD+aI61aFsHY2D9XcXvDe4eZamYk0xDB05iEGkxRsBY4+oU3fN43gows97dMkBnO0MEPuxjRsD1R7p8CctZ/AVZ1pIcFPPawZhL6+IdwLthBwSAHeSKi4HSBRwbEhhFvV5GpnCo6eqXD425fha8Fm1rlHDHuA6CBRNQllkjANHEII9LBMw/eQAtOi6NbE75GXS5vgkFaGsN2SrVwOqvy74PDNwxwcw1akuxnvlDtIXnrbBhLTpqmeBh+XaUxObCtA4gmOEOD2zXcELFemmqPnXcOfObpCg9iN3gDJYHhNEopxQmgavrd5TKKCwyvP4bOpY6NMozLHXt3BHF0JENXdokyyTukTdzMTom7vFtat8mp61wyQ6OBwi57FLVEPc143gqOrXCyzcU9fTrLFfyv9Kkh0TRJGm8QoBbeDUHW3JsXLuKvg2ND6DLmdabh89zFH1zKI/uKxQRLCxw9imjhMMvGmEG6Vy40bdqvyUOH3gtndIMjd5q0rolheBquDpDTATHA9RrMuv+WpcQv8uBGkwIaMblFwFLVQbsNJzzARV+292R6osNPgkK2X4r/CnDYcj+lqgNAJVUHSp/SLu2yaJARThI0Gefn4/Ihg4a4yR7PdKh9L1hmH64EK0+Xg6FoNYrcPCpIC1SSUSWJ2YqTnxWEalUk8NMnEG8nLRVpbFSND3gjTsL0pc3S9BnH7iI7/mrxB3MVMjmXsDQh4fiQMKlPhoB1X4Fr9uSbcRF4orYf3mAsPg7RNI+DWq3/ZXihnpsDhmz6JidjAZqjdtq53sewTMP5rykZxJ05qpTG6MQ2TBSm7B6ykBKYUYZa4HcY1KrCjnE/fl+2BMjsdDu1mzWGf9xQgthEx3K1dHsLdzL0mdyzORqJh8g9RjDxMZb/XOhHVrZre3YI8jWKF5GsLSOJoEg8Qhd75yoaKyNomYiMGoRdEnAyHbf1090arvEwjZRCvkVlOsuOpcKdMEhSu8gOen7E3qe1n1NY8OnPRx+Z7QeT2ggOHW9PpkN/CwMNSgPgN0XKS3ePfSn95p0cIOAxThPF7mlAQGSewIIwAkZsAB27+zPDqyB5o9REOSAESNFgqk5B+cSe6JxNbBJLQmiZmvoa6VdzEFBxB058CJGiE6N+rTEL6yzuwVuAY0cc3BjnCikTXx2oS06jMkYIjzMyHbksQ6mLD+iATSNT3bJKxBjWvc4xpgwI+BUc0K00ZJMp4mZmk0epfD4xZHicuCD3O40eCyKfMEWXGUwaJNFp2dysAJEliGmFk6lZFnuvh2hcrzkBEOkdnku0hNEkCmIYfASV+EhyURqsizbLxfYt+VnpGTbj3kbfK23BK6JWJfm5ViwQ8PxJKMBH22341rkunLfoIpBok+pjVz1hMhD0U0iduwynm6tlAW48RBQuMnrkwlTACSjApBUcjU5wCpJHRo+c+RIQ9XiN9KpOEcadi5i0M0AWir/ZC1K1KwdHo5A7T3ryND0vEK2ggEbfilChRqEBbj8k0AnWrUuaIOInuh6cM0pRh1JhkNekrb4mgScIwTkSQqG7V5NStata0pgBp1kia3C2VSXz6UzluGUAlgd3aNRCpzJGCo5kzmuZBmjqaBkigT9wKviCJtM4jBNMIo6EAE+GANFrV3BlNGaS541m7mqpJoE/cAlZNot+r2Rny0VDgpkIidrdqxXAO5TVTgLRq9ClIVkOf+E5jIPHEku5WjYYCm4KjVbOYulgtG1mNSca9Bn3lLWCEgJumTWgodxQUuD1T5mjlHKYM0srR1d0tnUniLN/1KjykblUKjlbPXsogLR9hnUlWQ195M1jLUnyShn5MozLHu1LmaMfcpQzSjlHW7jFuMekrb4LpvrcMEPDCaNjN7gmJ2pO9jUPY9lulAGnzkE/4DvldcQ3MJwQwareS3BT4vzgOjtxyJQ62+bG79nYpQIZg6if/gFxbfBu+Kw0CbwaJV7tQJgNKbhos2fw5vGYIHrerb5kCZAinf+J3yWPV3fC+6g4Yp0jqUvc6RlggwhjYxfXAKhwD527+FL4zhI/atbdOAZKQqR93O3k3V4HDEIGXeXhly5X4UkIerasf4/8DyaXaTz0Am2YAAAAASUVORK5CYII=','开源平台','00',1112,NULL,'2018-07-20 17:46:56','2019-01-31 10:08:22');

/*Table structure for table `platform_base_extend` */

DROP TABLE IF EXISTS `platform_base_extend`;

CREATE TABLE `platform_base_extend` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `company_name` varchar(100) DEFAULT NULL COMMENT '企业名称',
  `business_scope` text COMMENT '经营范围',
  `business_no` varchar(60) DEFAULT NULL COMMENT '工商注册号',
  `legal_person_name` varchar(32) DEFAULT NULL COMMENT '法人姓名',
  `legal_person_cred_type` varchar(6) DEFAULT NULL COMMENT '法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 ',
  `legal_person_cred_no` varchar(32) DEFAULT NULL COMMENT '法人证件号码',
  `business_licence` text COMMENT '营业执照',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `mobile` varchar(32) DEFAULT NULL COMMENT '企业座机',
  `fax` varchar(32) DEFAULT NULL COMMENT '企业传真',
  `http_address` text COMMENT '企业网址',
  `registered_capital` decimal(18,2) DEFAULT NULL COMMENT '注册资金',
  `company_address` text COMMENT '企业地址',
  `operator_id` int(11) DEFAULT NULL COMMENT '最后操作人编号',
  `operator_name` varchar(256) DEFAULT NULL COMMENT '最后操作人名称',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台扩展信息';

/*Data for the table `platform_base_extend` */

/*Table structure for table `platform_blockchain` */

DROP TABLE IF EXISTS `platform_blockchain`;

CREATE TABLE `platform_blockchain` (
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `block_hight` int(11) DEFAULT NULL COMMENT '区块高度',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '生成时间',
  `previousHash` varchar(255) DEFAULT NULL COMMENT '上一个区块hash',
  `hash` varchar(255) DEFAULT NULL COMMENT '区块hash',
  `reward_amout` decimal(18,2) DEFAULT NULL COMMENT '区块奖励数量',
  `amout` decimal(18,2) DEFAULT NULL COMMENT '区块交易金额'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `platform_blockchain` */

/*Table structure for table `platform_callback_url` */

DROP TABLE IF EXISTS `platform_callback_url`;

CREATE TABLE `platform_callback_url` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `callback_type` varchar(6) DEFAULT NULL COMMENT '回调类型   00：支付回调  01：退款回调  02：消息通知',
  `callback_url` text COMMENT '回调地址',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台回调地址配置';

/*Data for the table `platform_callback_url` */

/*Table structure for table `platform_coin_accout` */

DROP TABLE IF EXISTS `platform_coin_accout`;

CREATE TABLE `platform_coin_accout` (
  `platform_id` bigint(11) DEFAULT NULL COMMENT '平台编号',
  `platform_type` varchar(255) DEFAULT NULL COMMENT '平台类型：1 顶级平台 2 普通平台',
  `address` varchar(255) DEFAULT NULL COMMENT '平台合约地址',
  `account_type` varchar(10) DEFAULT NULL COMMENT '平台账号类型：1平台总量 2手续费',
  `account_balance` decimal(18,2) DEFAULT NULL COMMENT '账户余额',
  `freeze_balance` decimal(18,2) DEFAULT NULL COMMENT '冻结金额',
  `check_sum` varchar(40) DEFAULT NULL COMMENT '校验码（余额加密值）',
  `status` varchar(2) DEFAULT NULL COMMENT '账户状态 00：正常，01：冻结',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `platform_coin_accout` */

/*Table structure for table `platform_coin_accout_journal` */

DROP TABLE IF EXISTS `platform_coin_accout_journal`;

CREATE TABLE `platform_coin_accout_journal` (
  `journal_id` bigint(11) DEFAULT NULL COMMENT '交易流水id',
  `platform_id` bigint(11) DEFAULT NULL COMMENT '交易所属平台',
  `address` varchar(255) DEFAULT NULL COMMENT '交易地址',
  `trade_type` varchar(10) DEFAULT NULL COMMENT '交易类型：奖励、转账、手续费、',
  `in_out_flag` varchar(10) DEFAULT NULL COMMENT '来往标志  1：来账   2：往账',
  `amount` decimal(18,2) DEFAULT NULL COMMENT '交易金额',
  `customer_type` varchar(11) DEFAULT NULL COMMENT '客户类型 平台客户 普通客户'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `platform_coin_accout_journal` */

/*Table structure for table `platform_coin_config` */

DROP TABLE IF EXISTS `platform_coin_config`;

CREATE TABLE `platform_coin_config` (
  `platform_id` bigint(11) DEFAULT NULL COMMENT '平台编号',
  `platform_type` varchar(10) DEFAULT NULL COMMENT '1：顶级平台 2：普通平台',
  `name` varchar(20) DEFAULT NULL COMMENT '平台代币名称',
  `symbol` varchar(20) DEFAULT NULL COMMENT '代币的简称',
  `sum` bigint(11) DEFAULT NULL COMMENT '平台币总量'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `platform_coin_config` */

/*Table structure for table `platform_coin_customer` */

DROP TABLE IF EXISTS `platform_coin_customer`;

CREATE TABLE `platform_coin_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `real_name` varchar(5) DEFAULT NULL COMMENT '真实姓名',
  `id_card` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `address` text COMMENT '地址',
  `private_key` text COMMENT '私钥',
  `balance` decimal(18,2) DEFAULT NULL COMMENT '余额',
  `freeze_balance` decimal(18,2) DEFAULT NULL COMMENT '冻结金额',
  `check_sum` varchar(40) DEFAULT NULL COMMENT '校验码（余额加密值）',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台唯一用户表';

/*Data for the table `platform_coin_customer` */

/*Table structure for table `platform_coin_transaction_from` */

DROP TABLE IF EXISTS `platform_coin_transaction_from`;

CREATE TABLE `platform_coin_transaction_from` (
  `platform_id` bigint(11) DEFAULT NULL COMMENT '平台编号',
  `from_address` varchar(255) DEFAULT NULL COMMENT '出账地址',
  `amount` decimal(18,2) DEFAULT NULL COMMENT '交易金额',
  `transaction_type` varchar(10) DEFAULT NULL COMMENT '1：奖励 2：转账 3：手续费',
  `serial_number` bigint(11) DEFAULT NULL COMMENT '各自平台的交易排序编号',
  `hash` varchar(255) DEFAULT NULL COMMENT '交易hash',
  `block_hash` varchar(255) DEFAULT NULL COMMENT '交易所属区块hash',
  `sign` varchar(255) DEFAULT NULL COMMENT '交易签名'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `platform_coin_transaction_from` */

insert  into `platform_coin_transaction_from`(`platform_id`,`from_address`,`amount`,`transaction_type`,`serial_number`,`hash`,`block_hash`,`sign`) values (NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `platform_coin_transaction_to` */

DROP TABLE IF EXISTS `platform_coin_transaction_to`;

CREATE TABLE `platform_coin_transaction_to` (
  `platform_id` bigint(11) DEFAULT NULL COMMENT '平台编号',
  `to_address` varchar(255) DEFAULT NULL COMMENT '入账地址',
  `amount` decimal(18,4) DEFAULT NULL COMMENT '交易金额',
  `hash` bigint(11) DEFAULT NULL COMMENT '交易hash',
  `transaction_type` varchar(11) NOT NULL COMMENT '1：奖励 2：转账 3：手续费',
  PRIMARY KEY (`transaction_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `platform_coin_transaction_to` */

/*Table structure for table `platform_function` */

DROP TABLE IF EXISTS `platform_function`;

CREATE TABLE `platform_function` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `code` varchar(20) NOT NULL COMMENT '代码',
  `name` varchar(20) NOT NULL COMMENT '名称',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台功能';

/*Data for the table `platform_function` */

/*Table structure for table `platform_function_config` */

DROP TABLE IF EXISTS `platform_function_config`;

CREATE TABLE `platform_function_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `code` varchar(20) NOT NULL COMMENT '功能代码',
  `channel` varchar(20) DEFAULT NULL COMMENT '01 app 02小程序 03PC',
  `level` varchar(2) DEFAULT NULL COMMENT '级别',
  `isShow` varchar(2) DEFAULT NULL COMMENT '是否展示 0开 1关',
  `p_code` varchar(20) DEFAULT NULL COMMENT '父级菜单',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台功能配置';

/*Data for the table `platform_function_config` */

/*Table structure for table `platform_safety_config` */

DROP TABLE IF EXISTS `platform_safety_config`;

CREATE TABLE `platform_safety_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `app_id` varchar(256) NOT NULL COMMENT '平台编号密文',
  `app_key` text COMMENT '密钥（盐值）：在首次登录或者首次获取tiken的时候用到',
  `public_key` text COMMENT '公钥',
  `private_key` text COMMENT '私钥',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台安全配置信息';

/*Data for the table `platform_safety_config` */

/*Table structure for table `qrtz_calendars` */

DROP TABLE IF EXISTS `qrtz_calendars`;

CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `qrtz_calendars` */

/*Table structure for table `qrtz_cron_triggers` */

DROP TABLE IF EXISTS `qrtz_cron_triggers`;

CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(200) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `qrtz_cron_triggers` */

/*Table structure for table `qrtz_fired_triggers` */

DROP TABLE IF EXISTS `qrtz_fired_triggers`;

CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `qrtz_fired_triggers` */

/*Table structure for table `qrtz_job_details` */

DROP TABLE IF EXISTS `qrtz_job_details`;

CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `qrtz_job_details` */

/*Table structure for table `qrtz_locks` */

DROP TABLE IF EXISTS `qrtz_locks`;

CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `qrtz_locks` */

insert  into `qrtz_locks`(`SCHED_NAME`,`LOCK_NAME`) values ('RenrenScheduler','STATE_ACCESS'),('RenrenScheduler','TRIGGER_ACCESS');

/*Table structure for table `qrtz_paused_trigger_grps` */

DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;

CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `qrtz_paused_trigger_grps` */

/*Table structure for table `qrtz_scheduler_state` */

DROP TABLE IF EXISTS `qrtz_scheduler_state`;

CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `qrtz_scheduler_state` */

insert  into `qrtz_scheduler_state`(`SCHED_NAME`,`INSTANCE_NAME`,`LAST_CHECKIN_TIME`,`CHECKIN_INTERVAL`) values ('RenrenScheduler','SKY-20170821QVC1543832280117',1543832338387,15000);

/*Table structure for table `qrtz_simple_triggers` */

DROP TABLE IF EXISTS `qrtz_simple_triggers`;

CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `qrtz_simple_triggers` */

/*Table structure for table `qrtz_simprop_triggers` */

DROP TABLE IF EXISTS `qrtz_simprop_triggers`;

CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `qrtz_simprop_triggers` */

/*Table structure for table `qrtz_triggers` */

DROP TABLE IF EXISTS `qrtz_triggers`;

CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `qrtz_triggers` */

/*Table structure for table `refund_order` */

DROP TABLE IF EXISTS `refund_order`;

CREATE TABLE `refund_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pay_order_no` varchar(32) NOT NULL COMMENT '支付订单号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `merchant_id` varchar(60) DEFAULT NULL COMMENT '商户账号',
  `refund_order_no` varchar(32) DEFAULT NULL COMMENT '退款订单号',
  `refund_amt` decimal(18,2) DEFAULT NULL COMMENT '退款金额',
  `refund_fee` decimal(18,2) DEFAULT NULL COMMENT '退款手续费',
  `refund_time` datetime DEFAULT NULL COMMENT '退款时间',
  `refund_reason` text COMMENT '退款原因',
  `refund_method` varchar(10) DEFAULT NULL COMMENT '退款方式（跟支付方式对应）',
  `refund_status` varchar(10) DEFAULT NULL COMMENT '退款状态（00退款成功 01退款失败 02退款处理中）',
  PRIMARY KEY (`id`,`pay_order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='退款订单表';

/*Data for the table `refund_order` */

/*Table structure for table `refund_serial` */

DROP TABLE IF EXISTS `refund_serial`;

CREATE TABLE `refund_serial` (
  `serial_no` varchar(32) NOT NULL COMMENT '退款流水号',
  `refund_order_no` varchar(32) NOT NULL COMMENT '退款订单号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `merchant_id` varchar(60) DEFAULT NULL COMMENT '商户账号',
  `pay_channel_no` varchar(32) DEFAULT NULL COMMENT '支付渠道号',
  `pay_company_no` varchar(32) DEFAULT NULL COMMENT '支付公司编号',
  `refund_amt` decimal(18,2) DEFAULT NULL COMMENT '退款金额',
  `refund_fee` decimal(18,2) DEFAULT NULL COMMENT '退款手续费',
  `refund_status` varchar(10) DEFAULT NULL COMMENT '退款状态（00退款成功 01退款失败 02退款处理中）',
  PRIMARY KEY (`serial_no`,`refund_order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退款流水表';

/*Data for the table `refund_serial` */

/*Table structure for table `scene_info` */

DROP TABLE IF EXISTS `scene_info`;

CREATE TABLE `scene_info` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `scene_code` varchar(20) NOT NULL COMMENT '场景编号',
  `scene_name` varchar(30) NOT NULL COMMENT '场景名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='场景';

/*Data for the table `scene_info` */

insert  into `scene_info`(`id`,`scene_code`,`scene_name`) values (1,'SC0001','点餐'),(2,'SC0002','拼团'),(3,'SC0003','竞拍'),(4,'SC0004','每日秒杀'),(5,'SC0005','小程序商城'),(6,'SC0006','火钻充值'),(9,'SC0007','爱算');

/*Table structure for table `scene_pay_method_config` */

DROP TABLE IF EXISTS `scene_pay_method_config`;

CREATE TABLE `scene_pay_method_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `scene_code` varchar(20) NOT NULL COMMENT '场景编号',
  `method_code` varchar(2) NOT NULL COMMENT '支付方式编号 1微信 2支付宝 3火源 4火钻 5余额 6原力值 7医保卡',
  `method` varchar(20) NOT NULL COMMENT '支付方式代码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='场景支付方式配置';

/*Data for the table `scene_pay_method_config` */

insert  into `scene_pay_method_config`(`id`,`scene_code`,`method_code`,`method`) values (1,'SC0001','2','ALIPAY'),(2,'SC0001','1','WECHAT'),(3,'SC0001','4','FIREDRILL'),(4,'SC0006','1','WECHAT'),(5,'SC0005','4','FIREDRILL'),(6,'SC0005','1','WECHAT');

/*Table structure for table `schedule_job` */

DROP TABLE IF EXISTS `schedule_job`;

CREATE TABLE `schedule_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='定时任务';

/*Data for the table `schedule_job` */

/*Table structure for table `schedule_job_log` */

DROP TABLE IF EXISTS `schedule_job_log`;

CREATE TABLE `schedule_job_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务日志';

/*Data for the table `schedule_job_log` */

/*Table structure for table `system_dictionary` */

DROP TABLE IF EXISTS `system_dictionary`;

CREATE TABLE `system_dictionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `dict_type` varchar(20) DEFAULT NULL COMMENT '类型   accountType：账户类型',
  `dict_type_name` varchar(60) DEFAULT NULL,
  `dict_code` varchar(32) DEFAULT NULL COMMENT '码值',
  `dict_codet_name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

/*Data for the table `system_dictionary` */

/*Table structure for table `unfreeze_journal` */

DROP TABLE IF EXISTS `unfreeze_journal`;

CREATE TABLE `unfreeze_journal` (
  `journal_id` varchar(32) NOT NULL COMMENT '流水号',
  `freeze_id` varchar(32) NOT NULL COMMENT '冻结流水号',
  `order_no` varchar(32) NOT NULL COMMENT '订单号',
  `trade_type` varchar(2) NOT NULL COMMENT '交易类型（支付:01，退款:02，提现:03，充值:04）',
  `unfreeze_balance` decimal(18,2) NOT NULL COMMENT '解冻金额',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`journal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='解冻流水表';

/*Data for the table `unfreeze_journal` */

/*Table structure for table `zipkin_annotations` */

DROP TABLE IF EXISTS `zipkin_annotations`;

CREATE TABLE `zipkin_annotations` (
  `trace_id` bigint(20) NOT NULL COMMENT 'coincides with zipkin_spans.trace_id',
  `span_id` bigint(20) NOT NULL COMMENT 'coincides with zipkin_spans.id',
  `a_key` varchar(255) NOT NULL COMMENT 'BinaryAnnotation.key or Annotation.value if type == -1',
  `a_value` blob COMMENT 'BinaryAnnotation.value(), which must be smaller than 64KB',
  `a_type` int(11) NOT NULL COMMENT 'BinaryAnnotation.type() or -1 if Annotation',
  `a_timestamp` bigint(20) DEFAULT NULL COMMENT 'Used to implement TTL; Annotation.timestamp or zipkin_spans.timestamp',
  `endpoint_ipv4` int(11) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null',
  `endpoint_ipv6` binary(16) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null, or no IPv6 address',
  `endpoint_port` smallint(6) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null',
  `endpoint_service_name` varchar(255) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null',
  UNIQUE KEY `trace_id` (`trace_id`,`span_id`,`a_key`,`a_timestamp`) COMMENT 'Ignore insert on duplicate',
  KEY `trace_id_2` (`trace_id`,`span_id`) COMMENT 'for joining with zipkin_spans',
  KEY `trace_id_3` (`trace_id`) COMMENT 'for getTraces/ByIds',
  KEY `endpoint_service_name` (`endpoint_service_name`) COMMENT 'for getTraces and getServiceNames',
  KEY `a_type` (`a_type`) COMMENT 'for getTraces',
  KEY `a_key` (`a_key`) COMMENT 'for getTraces'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;

/*Data for the table `zipkin_annotations` */

/*Table structure for table `zipkin_dependencies` */

DROP TABLE IF EXISTS `zipkin_dependencies`;

CREATE TABLE `zipkin_dependencies` (
  `day` date NOT NULL,
  `parent` varchar(255) NOT NULL,
  `child` varchar(255) NOT NULL,
  `call_count` bigint(20) DEFAULT NULL,
  UNIQUE KEY `day` (`day`,`parent`,`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;

/*Data for the table `zipkin_dependencies` */

/*Table structure for table `zipkin_spans` */

DROP TABLE IF EXISTS `zipkin_spans`;

CREATE TABLE `zipkin_spans` (
  `trace_id` bigint(20) NOT NULL,
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `debug` bit(1) DEFAULT NULL,
  `start_ts` bigint(20) DEFAULT NULL COMMENT 'Span.timestamp(): epoch micros used for endTs query and to implement TTL',
  `duration` bigint(20) DEFAULT NULL COMMENT 'Span.duration(): micros used for minDuration and maxDuration query',
  UNIQUE KEY `trace_id` (`trace_id`,`id`) COMMENT 'ignore insert on duplicate',
  KEY `trace_id_2` (`trace_id`,`id`) COMMENT 'for joining with zipkin_annotations',
  KEY `trace_id_3` (`trace_id`) COMMENT 'for getTracesByIds',
  KEY `name` (`name`) COMMENT 'for getTraces and getSpanNames',
  KEY `start_ts` (`start_ts`) COMMENT 'for getTraces ordering and range'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;

/*Data for the table `zipkin_spans` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
